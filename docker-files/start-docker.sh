#!/bin/bash
cp -r /app/docker-files/examples /app/docker-share
chown -R user:user /app/docker-share

cp /app/docker-files/start_md2reflect_jupyter.sh /home/user
cp /app/docker-files/start_md2reflect_terminal.sh /home/user
chown -R user:user /home/user/start_md2reflect_jupyter.sh

#rm -r /app/docker-files

login -f user

