import os
import csv
import numpy as np
import matplotlib.pyplot

from IPython.display import Image
from IPython.display import display
from pymol import cmd

from .composition import list_molecules


def create_folder(dataset_name, autoname=False):
    """Creates a folder with the dataset file name, if it doesn't exist.

    Parameters:
        dataset_name (str): Dataset filename without path nor extension.
        autoname (bool): If the requested folder already exists, a number is
            added recursively to the folder name. Default is False.

    Returns:
        None
    """
    if not os.path.exists(dataset_name):
        os.makedirs(dataset_name)
    elif autoname:
        name_counter = 1
        alternate_name = dataset_name
        while os.path.exists(alternate_name):
            name_counter += 1
            alternate_name = dataset_name + "_" + str(name_counter)
        os.makedirs(alternate_name)


def save_file(X, Y, file_path):
    """Saves lists of X, Y values into a TSV file (tabular separated values).

    Parameters:
        X (list): Values of the x axis.
        Y (list): Values of the y axis.
        file_path (str): Desired path of the file to save.

    Returns:
        None
    """
    data_file = open(file_path, "w+", newline="")
    writer = csv.writer(data_file, delimiter="\t")
    for bin_index in range(len(X)):
        writer.writerow([X[bin_index], Y[bin_index]])
    data_file.close()


def save_number_density(
    number_density_elements_allframes, zbin_interval, folder_name_prefix="", show=True
):
    """Displays and saves simulation number density plots of each element.

    This function displays the number densities of all elements in the
    simulation, saves those graphs to disk in the subfolder "figures/elements",
    and also saves the data into the subfolder "data/elements".

    Parameters:
        number_density_elements_allframes (dictionary of lists): Dictionary
            with the elements present in the simulation, each with its list of
            the number density of atoms in each z bin.
        zbin_interval (float): The thickness of each bin in Å.
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    all_keys = list(number_density_elements_allframes.keys())
    zbins_number = len(number_density_elements_allframes[all_keys[0]])

    x1 = [i * zbin_interval for i in range(int(zbins_number))]

    create_folder(os.path.join(folder_name_prefix, "data", "elements"))
    create_folder(os.path.join(folder_name_prefix, "figures", "elements"))

    for key in number_density_elements_allframes:
        datafile = f"Number_density_{key}.tsv"
        imagefile = f"Number_density_{key}.png"

        save_file(
            x1,
            number_density_elements_allframes[key],
            os.path.join(folder_name_prefix, "data", "elements", datafile),
        )

        if show is True:
            matplotlib.pyplot.bar(
                x1,
                number_density_elements_allframes[key],
                width=zbin_interval,
                color=np.random.rand(3),
            )

            matplotlib.pyplot.title(key)
            matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
            matplotlib.pyplot.xlabel("z (Å)")
            matplotlib.pyplot.ylabel("Number density (1/Å^3)")
            matplotlib.pyplot.savefig(
                os.path.join(folder_name_prefix, "figures", "elements", imagefile)
            )
            matplotlib.pyplot.show(block=False)


def save_number_density_atom_types(
    number_density_atom_types_allframes, zbin_interval, folder_name_prefix="", show=True
):
    """Displays and saves simulation number density plots of each atom type.

    This function displays the number densities of all atom types in the
    simulation, saves those graphs to disk in the subfolder
    "figures/atom_types", and also saves the data into the subfolder
    "data/atom_types".


    Parameters:
        number_density_atom_types_allframes (dictionary of dictionaries):
            Dictionary with the molecules present in the simulation, each with
            its dictionary of atom types in that molecule, and where each atom
            type has a list with its number density of atoms in each z bin.
        zbin_interval (float): The thickness of each bin in Å.
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    molecular_keys = list(number_density_atom_types_allframes.keys())
    atom_keys = list(number_density_atom_types_allframes[molecular_keys[0]].keys())
    zbins_number = len(
        number_density_atom_types_allframes[molecular_keys[0]][atom_keys[0]]
    )

    x1 = [i * zbin_interval for i in range(int(zbins_number))]

    create_folder(os.path.join(folder_name_prefix, "data", "atom_types"))
    create_folder(os.path.join(folder_name_prefix, "figures", "atom_types"))

    for residue in number_density_atom_types_allframes:
        for atom_type in number_density_atom_types_allframes[residue]:
            datafile = f"Number_density_{residue}_{atom_type}.tsv"
            imagefile = f"Number_density_{residue}_{atom_type}.png"

            save_file(
                x1,
                number_density_atom_types_allframes[residue][atom_type],
                os.path.join(folder_name_prefix, "data", "atom_types", datafile),
            )

            if show is True:
                matplotlib.pyplot.bar(
                    x1,
                    (number_density_atom_types_allframes[residue][atom_type]),
                    width=zbin_interval,
                    color=np.random.rand(3),
                )
                matplotlib.pyplot.title(f"{residue} / {atom_type}")
                matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
                matplotlib.pyplot.xlabel("z (Å)")
                matplotlib.pyplot.ylabel("Number density (1/Å^3)")
                matplotlib.pyplot.savefig(
                    os.path.join(folder_name_prefix, "figures", "atom_types", imagefile)
                )
                matplotlib.pyplot.show(block=False)


def save_SLD_elements(SLD_elements, zbin_interval, folder_name_prefix="", show=True):
    """Displays and saves simulation scattering length density plots of elements.

    This function displays the real component of the scattering length density
    of all elements in the simulation, saves those graphs to disk in the
    subfolder "figures/elements", and also saves the data into the subfolder
    "data/elements".

    Parameters:
        SLD_elements (dictionary of lists): Dictionary with the elements
            present in the simulation, each with a list of the real part of its
            scattering length density in each z bin and another similar list
            with the imaginary part of its scattering length density.
        zbin_interval (float): The thickness of each bin in Å.
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    for element in SLD_elements:
        zbins_number = len(SLD_elements[element][0])

    x1 = [i * zbin_interval for i in range(zbins_number)]

    create_folder(os.path.join(folder_name_prefix, "data", "elements"))
    create_folder(os.path.join(folder_name_prefix, "figures", "elements"))

    for key in SLD_elements:
        datafile = f"SLD_{key}.tsv"
        imagefile = f"SLD_{key}.png"

        save_file(
            x1,
            [bin * 10 ** 6 for bin in SLD_elements[key][0]],
            os.path.join(folder_name_prefix, "data", "elements", datafile),
        )

        if show is True:
            matplotlib.pyplot.plot(
                x1,
                [bin * 10 ** 6 for bin in SLD_elements[key][0]],
                color=np.random.rand(3),
            )
            matplotlib.pyplot.title(key)
            matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
            matplotlib.pyplot.xlabel("z (Å)")
            matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
            matplotlib.pyplot.savefig(
                os.path.join(folder_name_prefix, "figures", "elements", imagefile)
            )
            matplotlib.pyplot.show(block=False)


def save_SLD_atom_types(
    SLD_atom_types, zbin_interval, sufix="", folder_name_prefix="", show=True
):
    """Displays and saves simulation scattering length density plots of atom types.

    This function displays the real component of the scattering length density
    of all atom types in the simulation, saves those graphs to disk in the
    subfolder "figures/atom_types", and also saves the data into the subfolder
    "data/atom_types".

    Parameters:
        SLD_atom_types (dictionary of dictionaries of lists): Dictionary with
            the molecules present in the simulation, each with its dictionary
            of atom types in that molecule, and where each atom type has a list
            with the real part of its scattering length density in each z bin
            and another similar list with the imaginary part of its scattering
            length density.
        zbin_interval (float): The thickness of each bin in Å.
        sufix (str): The suffix allows to add a string to the file names in
            order to distinguish the new files from the ones saved in previous
            calculations. The default value is an empty string "".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    for residue in SLD_atom_types:
        for atom_type in SLD_atom_types[residue]:
            zbins_number = len(SLD_atom_types[residue][atom_type][0])

    x1 = [i * zbin_interval for i in range(zbins_number)]

    create_folder(os.path.join(folder_name_prefix, "data", "atom_types"))
    create_folder(os.path.join(folder_name_prefix, "figures", "atom_types"))

    for residue in SLD_atom_types:
        for atom_type in SLD_atom_types[residue]:
            datafile = f"SLD_{residue}_{atom_type}{sufix}.tsv"
            imagefile = f"SLD_{residue}_{atom_type}{sufix}.png"

            save_file(
                x1,
                [bin * 10 ** 6 for bin in SLD_atom_types[residue][atom_type][0]],
                os.path.join(folder_name_prefix, "data", "atom_types", datafile),
            )

            if show is True:
                matplotlib.pyplot.plot(
                    x1,
                    [bin * 10 ** 6 for bin in SLD_atom_types[residue][atom_type][0]],
                    color=np.random.rand(3),
                )
                matplotlib.pyplot.title(residue + " / " + atom_type)
                matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
                matplotlib.pyplot.xlabel("z (Å)")
                matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
                matplotlib.pyplot.savefig(
                    os.path.join(folder_name_prefix, "figures", "atom_types", imagefile)
                )
                matplotlib.pyplot.show(block=False)


def save_SLD_total(
    SLD_total,
    SLD_total_no_substrate,
    zbin_interval,
    filename="SLD_total",
    folder_name_prefix="",
    show=True,
):
    """Displays and saves total simulation SLD plot with/out substrate.

    This function displays the real component of the total scattering length
    density of the simulation and also without the contribution of the
    substrate, saves those graphs to disk in the  subfolder
    "figures/atom_types", and also saves the data into the subfolder
    "data/atom_types".

    Parameters:
        SLD_total (tuple of lists): Total scattering length density
            of the simulation in each z bin (real and imaginary parts).
        SLD_total_no_substrate (tuple of lists): Total scattering length
            density of the simulation in each z bin without the contribution of
            the substrate element/residues/molecules (real and imaginary
            parts).
        zbin_interval (float): The thickness of each bin in Å.
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "SLD_total".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    zbins_number = len(SLD_total[0])

    x1 = [i * zbin_interval for i in range(int(zbins_number))]

    create_folder(os.path.join(folder_name_prefix, "data"))
    create_folder(os.path.join(folder_name_prefix, "figures"))

    save_file(
        x1,
        [bin * 10 ** 6 for bin in SLD_total[0]],
        os.path.join(folder_name_prefix, "data", filename + ".tsv"),
    )

    if show is True:
        matplotlib.pyplot.plot(
            x1, [bin * 10 ** 6 for bin in SLD_total[0]], color="black"
        )
        matplotlib.pyplot.title("total")
        matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
        matplotlib.pyplot.xlabel("z (Å)")
        matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)

    x1 = [i * zbin_interval for i in range(int(zbins_number))]

    save_file(
        x1,
        [bin * 10 ** 6 for bin in SLD_total_no_substrate[0]],
        os.path.join(folder_name_prefix, "data", filename + "_no_substrate.tsv"),
    )
    if show is True:
        matplotlib.pyplot.plot(
            x1, [bin * 10 ** 6 for bin in SLD_total_no_substrate[0]], color="black"
        )
        matplotlib.pyplot.title("total without substrate")
        matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
        matplotlib.pyplot.xlabel("z (Å)")
        matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + "_no_substrate.png")
        )
        matplotlib.pyplot.show(block=False)


def save_SLD_left(
    SLD_tuned_left,
    substrate_layers,
    environment_layers,
    zbin_interval,
    semi_infinite_zbins=20,
    filename="SLD_tuned_left",
    folder_name_prefix="",
    show=True,
):
    """Displays & saves SLD plot with left side of simulation and added layers.

    This function displays the real component of the scattering length density
    of the left hand side of the simulation with the added theoretical
    substrate and environment layers, saves those graphs to disk in the
    subfolder "figures", and it also saves the data into the subfolder "data".

    'SLD_tuned_left' has one scattering length density value for the substrate,
    one for each additional layer, all the points of the simulation, and one
    for the environment. Hence, it needs the 'substrate_layers' and/or the
    'environment_layers' object as input in order to determine the structural
    information of the intermediate layers such as their thickness.

    Parameters:
        SLD_tuned_left (tuple of numpy arrays): Constructed scattering length
            density to feed the reflectivity calculation, that combines the
            relevant section of the simulation scattering length density with
            the values of the manually added substrate and environment layers
            (with both, real and imaginary components).
        substrate_layers (list of tuples): List with one or more tuple elements
            containing the information of each material layer (real part of the
            the scattering length density, imaginary part of the scattering
            length density, thickness, and roughness) that will be added to the
            substrate. The first one is the semi-infinite layer and its
            thickness should be set to zero.
        environment_layers (list of tuples): List with one or more tuple
            elements containing the information of each material layer (real
            part of the scattering length density, imaginary part of the
            scattering length density, thickness, and roughness) that will be
            added to the oposite side to the substrate. The first one is the
            semi-infinite layer and its thickness should be set to zero.
        zbin_interval (float): The thickness of each bin in Å.
        semi_infinite_zbins (int): Number of points that will be added to the
            left and the right hand sides of the profile to represent the semi-
            infinite layers in the graph.
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "SLD_tuned_left".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    # Number of substrate layers (including the semiinfinite substrate layer)
    number_substrate_layers = len(substrate_layers)
    number_environment_layers = len(environment_layers)

    SLD_simulation = (
        SLD_tuned_left[0][number_substrate_layers:-number_environment_layers],
        SLD_tuned_left[1][number_substrate_layers:-number_environment_layers],
    )

    first_layer = True
    for layer in substrate_layers:
        if first_layer is True:
            first_layer = False
            substrate_layers_zbins = semi_infinite_zbins  # number of bins
            y_substrate_layers = [
                substrate_layers[0][0] * 10 ** 6
            ] * semi_infinite_zbins  # bin values

        else:
            # number of bins
            substrate_layers_zbins += int(layer[2] / zbin_interval)
            y_substrate_layers += [layer[0] * 10 ** 6] * int(
                layer[2] / zbin_interval
            )  # bin values

    first_layer = True
    for layer in environment_layers:
        if first_layer is True:
            first_layer = False
            environment_layers_zbins = semi_infinite_zbins  # number of bins
            y_environment_layers = [
                environment_layers[0][0] * 10 ** 6
            ] * semi_infinite_zbins  # bin values

        else:
            # number of bins
            environment_layers_zbins += int(layer[2] / zbin_interval)
            y_environment_layers = [layer[0] * 10 ** 6] * int(
                layer[2] / zbin_interval
            ) + y_environment_layers  # bin values

    simulation_zbins = len(SLD_simulation[0])
    total_zbins = substrate_layers_zbins + simulation_zbins + environment_layers_zbins

    x1 = [i * zbin_interval for i in range(total_zbins)]
    y1 = (
        y_substrate_layers
        + [bin * 10 ** 6 for bin in SLD_simulation[0]]
        + y_environment_layers
    )

    create_folder(os.path.join(folder_name_prefix, "data"))
    create_folder(os.path.join(folder_name_prefix, "figures"))

    save_file(x1, y1, os.path.join(folder_name_prefix, "data", filename + ".tsv"))

    if show is True:
        matplotlib.pyplot.plot(x1, y1)
        matplotlib.pyplot.title("total SLD with manual substrate/environment (left)")
        matplotlib.pyplot.xlim(0, total_zbins * zbin_interval)
        matplotlib.pyplot.xlabel("z (Å)")
        matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)


def save_SLD_right(
    SLD_tuned_right,
    substrate_layers,
    environment_layers,
    zbin_interval,
    semi_infinite_zbins=20,
    filename="SLD_tuned_right",
    folder_name_prefix="",
    show=True,
):
    """Displays and saves SLD plot built with layers plus right simulation.

    This function displays the real component of the scattering length density
    of the right hand side of the simulation with the added theoretical
    substrate and environment layers, saves those graphs to disk in the
    subfolder "figures", and it also saves the data into the subfolder "data".

    'SLD_tuned_left' has one scattering length density value for the substrate,
    one for each additional layer, all the points of the simulation, and one
    for the environment. Hence, it needs the 'substrate_layers' and/or the
    'environment_layers' object as input in order to determine the structural
    information of the intermediate layers such as their thickness.

    Parameters:
        SLD_tuned_right (tuple of numpy arrays): Constructed scattering length
            density to feed the reflectivity calculation, that combines the
            relevant section of the simulation scattering length density with
            the values of the manually added substrate and environment layers
            (with both, real and imaginary components).
        substrate_layers (list of tuples): List with one or more tuple elements
            containing the information of each material layer (real part of the
            the scattering length density, imaginary part of the scattering
            length density, thickness, and roughness) that will be added to the
            substrate. The first one is the semi-infinite layer and its
            thickness should be set to zero.
        environment_layers (list of tuples): List with one or more tuple
            elements containing the information of each material layer (real
            part of the scattering length density, imaginary part of the
            scattering length density, thickness, and roughness) that will be
            added to the oposite side to the substrate. The first one is the
            semi-infinite layer and its thickness should be set to zero.
        zbin_interval (float): The thickness of each bin in Å.
        semi_infinite_zbins (int): Number of points that will be added to the
            left and the right hand sides of the profile to represent the semi-
            infinite layers in the graph.
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "SLD_tuned_right".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    # Number of substrate layers (including the semiinfinite substrate layer)
    number_substrate_layers = len(substrate_layers)
    number_environment_layers = len(environment_layers)

    SLD_simulation = (
        SLD_tuned_right[0][number_environment_layers:-number_substrate_layers],
        SLD_tuned_right[1][number_environment_layers:-number_substrate_layers],
    )

    first_layer = True
    for layer in substrate_layers:
        if first_layer is True:
            first_layer = False
            substrate_layers_zbins = semi_infinite_zbins  # number of bins
            y_substrate_layers = [
                substrate_layers[0][0] * 10 ** 6
            ] * semi_infinite_zbins  # bin values

        else:
            # number of bins
            substrate_layers_zbins += int(layer[2] / zbin_interval)
            y_substrate_layers = [layer[0] * 10 ** 6] * int(
                layer[2] / zbin_interval
            ) + y_substrate_layers  # bin values

    first_layer = True
    for layer in environment_layers:
        if first_layer is True:
            first_layer = False
            environment_layers_zbins = semi_infinite_zbins  # number of bins
            y_environment_layers = [
                environment_layers[0][0] * 10 ** 6
            ] * semi_infinite_zbins  # bin values

        else:
            # number of bins
            environment_layers_zbins += int(layer[2] / zbin_interval)
            # bin values
            y_environment_layers += [layer[0] * 10 ** 6] * int(layer[2] / zbin_interval)

    simulation_zbins = len(SLD_simulation[0])
    total_zbins = environment_layers_zbins + simulation_zbins + substrate_layers_zbins

    x1 = [i * zbin_interval for i in range(total_zbins)]
    y1 = (
        y_environment_layers
        + [bin * 10 ** 6 for bin in SLD_simulation[0]]
        + y_substrate_layers
    )

    create_folder(os.path.join(folder_name_prefix, "data"))
    create_folder(os.path.join(folder_name_prefix, "figures"))

    save_file(x1, y1, os.path.join(folder_name_prefix, "data", filename + ".tsv"))

    if show is True:
        matplotlib.pyplot.plot(x1, y1)
        matplotlib.pyplot.title(
            "total SLD with manual substrate/environment " "(right)"
        )
        matplotlib.pyplot.xlim(0, total_zbins * zbin_interval)
        matplotlib.pyplot.xlabel("z (Å)")
        matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)


def save_SLD_atom_types_combination(
    SLD_atom_types_combination,
    zbin_interval,
    filename="SLD_atom_types_combination",
    folder_name_prefix="",
    show=True,
):
    """Displays and saves the SLD plot of any arbitrary subset of atom types.

    This function displays the real component of the scattering length density
    of any arbitrary subset of atom types, saves those graphs to disk in the
    subfolder "figures", and it also saves the data into the subfolder "data".

    Parameters:
        SLD_atom_types_combination (tuple of lists): Real component of the
            scattering length density contribution of the selected atom types
            in each z bin, as well as the imaginary component.
        zbin_interval (float): The thickness of each bin in Å.
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "SLD_atom_types_combination".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    zbins_number = len(SLD_atom_types_combination[0])

    x1 = [i * zbin_interval for i in range(zbins_number)]

    create_folder(os.path.join(folder_name_prefix, "data"))
    create_folder(os.path.join(folder_name_prefix, "figures"))

    save_file(
        x1,
        [bin * 10 ** 6 for bin in SLD_atom_types_combination[0]],
        os.path.join(folder_name_prefix, "data", filename + ".tsv"),
    )

    if show is True:
        matplotlib.pyplot.plot(
            x1, [bin * 10 ** 6 for bin in SLD_atom_types_combination[0]], color="black"
        )
        matplotlib.pyplot.title("atom types combination")
        matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
        matplotlib.pyplot.xlabel("z (Å)")
        matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)


def save_reflectivity_left(
    reflectivity_MD_left,
    q_max=0.25,
    q_number_points=1000,
    filename="R_tuned_left",
    folder_name_prefix="",
    show=True,
):
    """Display and save reflectivity plot corresponding to left side of SLD.

    This function displays the neutron or x-ray reflectivities, saves this
    graph to disk in the subfolder "figures", and it also saves the data into
    the subfolder "data"

    Parameters:
        reflectivity_MD_left (numpy array): Reflectivity profile as a function
            of the momentum transfer (Å^-1) corresponding to combining the left
            hand side of the scattering length density of the simulation with
            theoretical substrate and environment layers.
        q_max (float): Maximum value of momentum transfer (x axis of the
            reflectivity curve) to which calculate the reflectivity. The
            default value is 0.25 Å^-1.
        q_number_points (int): Number of desired points of the reflectivity
            curve (q is the momentum transfer). The default value is 1000
            points.
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "R_tuned_left".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    create_folder(os.path.join(folder_name_prefix, "data"))
    create_folder(os.path.join(folder_name_prefix, "figures"))

    q_left = np.linspace(0, q_max, q_number_points)

    save_file(
        q_left,
        reflectivity_MD_left,
        os.path.join(folder_name_prefix, "data", filename + ".tsv"),
    )

    if show is True:
        matplotlib.pyplot.semilogy(q_left, reflectivity_MD_left)
        matplotlib.pyplot.title(
            "Reflectivity with manual " "substrate/environment (left)"
        )
        matplotlib.pyplot.xlabel("$q_z$ (${Å}^{-1}$)")
        matplotlib.pyplot.ylabel("Reflectivity")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)


def save_reflectivity_right(
    reflectivity_MD_right,
    q_max=0.25,
    q_number_points=1000,
    filename="R_tuned_right",
    folder_name_prefix="",
    show=True,
):
    """Display and save reflectivity plot corresponding to right side of SLD.

    This function displays the neutron or x-ray reflectivities, saves this
    graph to disk in the subfolder "figures", and it also saves the data into
    the subfolder "data".

    Parameters:
        reflectivity_MD_right (numpy array): Reflectivity profile as a function
            of the momentum transfer (Å^-1) corresponding to combining the left
            hand side of the scattering length density of the simulation with
            theoretical substrate and environment layers.
        q_max (float): Maximum value of momentum transfer (x axis of the
            reflectivity curve) to which calculate the reflectivity. The
            default value is 0.25 Å^-1.
        q_number_points (int): Number of desired points of the reflectivity
            curve (q is the momentum transfer). The default value is 1000
            points.
        q_max (float): Maximum value of momentum transfer (x axis of the
            reflectivity curve) to which calculate the reflectivity. The
            default value is 0.25 Å^-1.
        q_number_points (int): Number of desired points of the reflectivity
            curve (q is the momentum transfer). The default value is 1000
            points.
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "R_tuned_right".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        None
    """
    q_right = np.linspace(0, q_max, q_number_points)

    create_folder(os.path.join(folder_name_prefix, "data"))
    create_folder(os.path.join(folder_name_prefix, "figures"))

    save_file(
        q_right,
        reflectivity_MD_right,
        os.path.join(folder_name_prefix, "data", filename + ".tsv"),
    )

    if show is True:
        matplotlib.pyplot.semilogy(q_right, reflectivity_MD_right)
        matplotlib.pyplot.title(
            "Reflectivity with manual " "substrate/environment (right)"
        )
        matplotlib.pyplot.xlabel("$q_z$ (${Å}^{-1}$)")
        matplotlib.pyplot.ylabel("Reflectivity")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)


def display_residues(traj, residues_to_find=None, folder_name="residues", show=True):
    """Displays molecules with the labels of its atoms.

    This function draws the molecules in the simulation with the label of each
    atom type next to it, so that those can be identified more easily. It's
    particularly useful when carrying out :func:`isotopic substitution` and the
    deuterated atoms types need to be specified.

    It also saves drawings and PDB files of the molecules in the subfolder
    "residues".

    Parameters:
        traj (trajectory): The loaded simulation trajectory.
        residues_to_find (list of str): List with the names of the residues,
            molecules or single atoms to draw. The default value is None, in
            which case it displays an image of all of them. When only one is
            selected, this one is drawn from four different angles, to ensure
            that all atoms are visible from at least one of them.
        folder_name (str): Folder path to create and store the files. The
            default is to create the folder in the working directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ or simply be saved on disk. Default value is True.

    Returns:
        None

    Example:
    ::

        display_residues(traj, residues_to_find = ["bmim", "CU"])
    """
    create_folder(folder_name)

    # Without any specified molecule, it displays all molecules
    if residues_to_find is None:
        residues_to_find = [residue[0] for residue in list_molecules(traj)]
    # Converts one molecule to a list with one molecule
    elif type(residues_to_find) == str:
        residues_to_find = [residues_to_find]

    print(residues_to_find)

    # Selects the atoms in that residue
    for item in residues_to_find:
        atoms_to_keep = []
        for residue in traj.topology.residues:
            if residue.name == item:
                atoms_to_keep = []
                for atom in residue.atoms:
                    atoms_to_keep += [atom.index]
                break
        reduced_trajectory = traj[0].atom_slice(atoms_to_keep)
        string_save_pdb = os.path.join(folder_name, item + "_residue.pdb")
        reduced_trajectory.save_pdb(string_save_pdb)

        cmd.delete("all")
        string_cmd_load = os.path.join(folder_name, item + "_residue.pdb")
        cmd.load(string_cmd_load)
        cmd.orient()
        cmd.show("sticks")
        cmd.set("valence", "on")
        cmd.set("stick_ball", "on")
        cmd.set("stick_ball_ratio", 3)
        cmd.set("stick_radius", 0.2)
        cmd.label(selection="all", expression="name")
        cmd.set("label_color", "black")
        cmd.set("label_font_id", 7)
        cmd.set("label_size", -0.2)

        # For a single molecule show 4 sides
        if len(residues_to_find) == 1:
            for rot_angle in range(0, 360, 90):
                cmd.rotate(axis="x", angle=rot_angle)
                string_cmd_png = os.path.join(
                    folder_name, item + "_" + str(rot_angle) + ".png"
                )
                cmd.png(string_cmd_png)  # ray = 1
                if show is True:
                    string_display_image = os.path.join(
                        folder_name, item + "_" + str(rot_angle) + ".png"
                    )
                    display(Image(filename=string_display_image))

        # For multiple molecules show only one side of each molecule
        else:
            string_cmd_png2 = os.path.join(folder_name, item + ".png")
            cmd.png(string_cmd_png2)  # ray = 1
            if show is True:
                string_display_image2 = os.path.join(folder_name, item + ".png")
                display(Image(filename=string_display_image2))


def show_SLD_range(
    SLD_profile,
    zbin_interval,
    starting_point=None,
    ending_point=None,
    distance_input=True,
    filename="SLD_range",
    folder_name_prefix="",
    show=True,
):
    """Allows to visualize and select the range of interest in the SLD.

    This function displays ranges of the scattering length density to better
    select and visualize them, and saves this graph to disk in the subfolder
    "figures".

    The range can be indicated with the z bin indexs or their positions. In the
    latter case, 'distance_input' must be set to True. Either way, the function
    will also present the equivalence between bin index and z axis positions of
    the range, so that this information can be used thereafter in any other
    functions of the module.

    Parameters:
        SLD_profile (list): Real contribution of the scattering length density.
        zbin_interval (float): The thickness of each bin in Å.
        starting_point (int or float): Starting point of the range to average.
            It can be its z bin index or its position on the z axis. The
            'distance_input' parameter must be set accordingly. The default
            value is None.
        ending_point (int or float): Ending point of the range to average. It
            can be its z bin index or its position on the z axis. The
            'distance_input' parameter must be set accordingly. The default
            value is None.
        distance_input (bool): Indicates if the range is indicated by its z bin
            indexs (False) or their position in Å (True, the default).
        filename (str): The file name is added to all files to distinguish them
            from similar graphs saved previously. The default value is
            "SLD_range".
        folder_name_prefix (str): Folder to create the subfolders to store the
            files. The default is to create the subfolders in the working
            directory.
        show (bool): Controls whether graphs should be shown on the display in
            situ (True) or simply be saved on disk (False). Default value is
            True.

    Returns:
        starting_zbin or starting_point (int or float),
        ending_zbin or ending_point (int or float) and
        ending_zbin-starting_zbin+1 or ending_point-starting_point (int or
        float): The starting and ending points, as well as the interval
        equivalence in z bin index if they were distances and the other way
        around.
    """
    zbins_number = len(SLD_profile)
    x1 = [i * zbin_interval for i in range(int(zbins_number))]

    # Convert zbin indexs to positions, if necessary
    if distance_input is False:
        if starting_point is not None:
            starting_zbin = starting_point
            starting_point = zbin_interval * starting_point
        if ending_point is not None:
            ending_zbin = ending_point
            ending_point = zbin_interval * ending_point

    # Check that point positions are ok and change them otherwise
    if starting_point is None or starting_point < x1[0]:
        starting_point = x1[0]
    if ending_point is None or ending_point > x1[-1]:
        ending_point = x1[-1]

    if starting_point > ending_point:
        temp_point = starting_point
        starting_point = ending_point
        ending_point = temp_point

    # Compute the zbins corresponding to the specified distance positions,
    # if necessary
    if distance_input is True:
        starting_zbin = int(starting_point / zbin_interval)
        ending_zbin = int(ending_point / zbin_interval)

    # Compute the maximum value of y
    y_min = min(SLD_profile)
    y_max = max(SLD_profile)

    if show is True:
        create_folder(os.path.join(folder_name_prefix, "figures"))
        matplotlib.pyplot.plot(
            x1, [bin * 10 ** 6 for bin in SLD_profile], color="black"
        )

        matplotlib.pyplot.bar(
            [starting_point + (ending_point - starting_point) / 2],
            [(y_max - y_min) * (10 ** 6)],
            width=ending_point - starting_point,
            bottom=(y_min * (10 ** 6)),
            color="red",
            alpha=0.5,
        )

        matplotlib.pyplot.title("SLD profile")
        matplotlib.pyplot.xlim(0, zbins_number * zbin_interval)
        matplotlib.pyplot.ylim(
            (y_min * 10 ** 6) - (0.04 * (y_max - y_min) * (10 ** 6)),
            (y_max * 10 ** 6) + (0.04 * (y_max - y_min) * (10 ** 6)),
        )
        matplotlib.pyplot.xlabel("z (Å)")
        matplotlib.pyplot.ylabel("SLD (10^-6 · Å^-2)")
        matplotlib.pyplot.savefig(
            os.path.join(folder_name_prefix, "figures", filename + ".png")
        )
        matplotlib.pyplot.show(block=False)

    if distance_input is True:
        print(f"Start: position = {starting_point} Å, index = {starting_zbin}")
        print(f"End: position = {ending_point} Å, index = {ending_zbin}")
        print(
            f"\nInterval: position = {ending_point - starting_point} Å, "
            f"index = {ending_zbin - starting_zbin + 1}"
        )
        return starting_zbin, ending_zbin, ending_zbin - starting_zbin + 1
    else:
        print(f"Start: index = {starting_zbin}, position = {starting_point} Å")
        print(f"End: index = {ending_zbin}, position = {ending_point} Å")
        print(
            f"\nInterval: index = {ending_zbin - starting_zbin + 1}, "
            f"position = {ending_point - starting_point} Å"
        )

        return starting_point, ending_point, ending_point - starting_point
