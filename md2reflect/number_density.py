import copy
from datetime import datetime

from .composition import find_atom_types, get_elements


def select_zbinning(traj, zbin_interval=0.5, zbins_number=None):
    """Establishes the binning which will be used to count atom positions.

    The binning can be established either by specifing the width that bins
    should have with 'zbin_interval' or by fixing the total number of bins.

    A moderate number of thicker bins provides an implicit averaging, with less
    noisy profiles, but also with less visible features. Large bins also pose a
    problem when the border between bins coincides with the middle of a peak,
    because the peak is then shared between the two bins, which can greatly
    reduce its height. This translates into a large variation of peak heights
    with only small variations of the bin thickness, a can be clearly observed
    in many systems for a large range of binning intervals.

    Conversely, a large number of thinner bins could provide more details of
    the profile features, and they do not pose so much danger of
    assymmetrically distorting the profile. However, they do not provide such
    a significant averaging and the resulting profiles are sometimes extremely
    noisy, which makes it harder to analyze its features.

    If a 'zbins_number' value is specified, it completely overrides any
    'zbin_interval'.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.
        zbin_interval (float): The thickness of each bin in Å (default value is
            0.5 Å).
        zbins_number (int): Total number of bins in the z axis in which the
            simulation box must be divided.

    Returns:
        - zbin_interval (float) - The thickness of each bin in Å.
        - box_zlength (float) - Total length of the simulation box in the
          direction of the z axis in Å.
        - zbins_number (int) - Total number of bins in the z axis in which the
          simulation box must be divided.
        - zbin_volume (float) - Volume of each thin bin layer in Å^3, taking
          into account the length of the simulation box in the direction of
          the x and y axis and the bin thickness in the direction of the z
          axis 'zbin_interval'.
    """
    # TODO(Muriel): We should probably get rid of the last bin if it's smaller
    # than the rest.

    # Define the binning in the z direcion for the density distribution
    # (box size of trajectory in z direction in Å)
    box_zlength = traj.unitcell_lengths[0][2] * 10

    if zbins_number is None or zbins_number <= 1:
        zbins_number = int(box_zlength / zbin_interval)  # number of z bins
    else:
        zbin_interval = box_zlength / zbins_number

    # Bin volume
    zbin_volume = (
        traj.unitcell_lengths[0][0] * traj.unitcell_lengths[0][1] * zbin_interval * 100
    )  # in Å

    print(
        f"Length of simulation box in x direction:  {traj.unitcell_lengths[0][0] * 10} Å"
    )
    print(
        f"Length of simulation box in y direction:  {traj.unitcell_lengths[0][1] * 10} Å"
    )
    print(f"Length of simulation box in z direction:  {box_zlength} Å \n")

    print(f"Thickness of z slices:  {zbin_interval} Å")
    print(f"Number of z bins:  {zbins_number}")
    print(f"Volume of z bins:  {zbin_volume} Å^3")

    return zbin_interval, box_zlength, zbins_number, zbin_volume


def count_number_atoms(traj, zbins_number):
    """Counts the number of atoms of each element as a function of the z axis.

    This function determines the different elements in the simulation, divides
    the simulation box in the specified number of bins along the z axis, counts
    how many atoms of each element are in each bin, and then averages them for
    all frames in the trajectory.

    This is the demanding calculation in the process, and it can take a while
    for large trajectories or when performed in systems with low resources.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.
        zbins_number (int): Total number of bins in the z axis in which the
          simulation box has been divided.

    Returns:
        number_atoms_elements_allframes (dictionary of lists): Dictionary with
        the elements present in the simulation, each with its list of the
        number of atoms located in each z bin.

    Example:
    ::

        count_number_atoms(traj, zbins_number)

        >>> 0:00:00.553642 (hh:mm:ss.ss) for one frame.
            There are 2 frames left.
            Estimated running time: 0:00:01.660926 (hh:mm:ss.ss)

            Actual running time: 0:00:01.570540 (hh:mm:ss.ss)

            {'hydrogen': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 1.6666666666666665, 18.666666666666668, 77.99999999999997,
              102.66666666666629, 72.66666666666671, 72.00000000000006, etc.],
             'carbon': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 3.0, 59.00000000000014, 101.33333333333297,
              50.66666666666675, 28.999999999999964, 26.99999999999997, etc.],
             'nitrogen': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 6.333333333333331, 35.99999999999998,
              14.000000000000007, 8.33333333333333, 5.666666666666665, etc.],
             'copper': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              399.9999999999947, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              etc],
             'bromine': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 10.0, 14.000000000000007, 5.999999999999998,
              2.6666666666666665, 2.333333333333333, 1.6666666666666665, etc.]}

        # Note: For the sake of clarity, only the first bins of each element
        # are shown here. Here it is evidenced that copper is the electrode
        # element by the fact that all its atoms are concentrated in one bin.
    """
    time_start = datetime.now()
    # Take trajectory and determine different_elements and box_zlength:
    different_elements = get_elements(traj)
    # box size of trajectory in z direction in Å
    box_zlength = traj.unitcell_lengths[0][2] * 10

    # Iteration for all atoms (it took 1h30 for file with 10000 frames)

    # Create dictionary with the empty lists for the number of atoms of each
    # element in each zbin
    number_atoms_elements_allframes = {
        different_elements[index_element].name: [0] * int(zbins_number)
        for index_element in range(len(different_elements))
    }

    # Iteration for all frames
    num_frames = len(traj)
    counter_frames = 0
    for frame in traj:
        counter_frames += 1.0

        # Iteration for all atoms in frame
        for atom_index in range(frame.n_atoms):

            # takes z coordinate of the atom in Å
            atom_zcoordinate = frame.xyz[0, atom_index, 2] * 10
            # determines to which bin it corresponds
            zbin_id = int((atom_zcoordinate / box_zlength) * zbins_number)
            # determines element name of the atom
            name_element = frame.topology.atom(atom_index).element.name

            # adds 1 atom to the counting normalized to the number of frames
            (number_atoms_elements_allframes[name_element][zbin_id]) += 1 / num_frames

        if counter_frames == 1.0:
            time_iteration = datetime.now() - time_start
            total_time = time_iteration * num_frames
            print(
                f"{time_iteration} (hh:mm:ss.ss) for one frame. \n"
                f"There are {num_frames-1} frames left. \n"
                f"Estimated running time: {total_time} (hh:mm:ss.ss)\n"
            )

    real_time = datetime.now() - time_start
    print(f"Actual running time: {real_time} (hh:mm:ss.ss)\n")
    return number_atoms_elements_allframes


def count_number_atoms_chunks(trajectory_chunks_generator, zbins_number):
    """Counts the number of atoms of each element along the z axis by chunks.

    This function determines the different elements in the simulation, divides
    the simulation box in the specified number of bins along the z axis, counts
    how many atoms of each element are in each bin, and then averages them for
    all frames in the trajectory.

    However, instead of calculating this by loading the whole trajectory at
    once, it allows to use a trajectory chunk generator that loads the
    trajectory bit by bit only a few frames at a time.

    This is the demanding calculation in the process, and it can take a while
    for large trajectories or when performed in systems with low resources. But
    with this function you can ensure that, even if it takes a long time, at
    least the calculation can be performed in modest systems, where the memory
    could otherwise become easily overloaded.

    Parameters:
        trajectory_chunks_generator (trajectory generator): Generator that
            yields the trajectory frames in smaller chunks.
        zbins_number (int): Total number of bins in the z axis in which the
          simulation box has been divided.

    Returns:
        number_atoms_elements_allframes (dictionary of lists): Dictionary with
        the elements present in the simulation, each with its list of the
        number of atoms located in each z bin.


    Example:
    ::

        count_number_atoms_chunks(trajectory_chunks, zbins_number)

        >>> 0:00:01.065247 (hh:mm:ss.ss) for one frame.

            Number of chunks:  5.0 Total number of frames:  17.0
            Actual running time: 0:00:09.898852 (hh:mm:ss.ss)

            {'hydrogen':
             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              0.0, 0.0, 0.9, 14.45, 71.75, 117.7, 80.45, 68.65, 81.1, 67.3,
              59.55, 63.65, 70.95, 74.2, 75.45, 70.25, 75.3, 67.25, etc.],
             'carbon':
             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              0.0, 0.0, 0.0, 0.0, 0.55, 42.7, 119.45, 60.9, 32.35, 24.15,
              17.85, 15.65, 30.35, 49.5, 56.4, 46.35, 41.7, 32.65, etc.],
             'nitrogen':
             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              0.0, 0.0, 0.0, 0.0, 0.0, 2.6, 36.8, 20.65, 7.75, 3.4, 2.0, 2.35,
              5.75, 14.85, 17.25, 11.95, 10.0, 8.15, 6.85, etc.],
             'copper':
             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              0.0, 400.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, etc.],
             'bromine':
             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
              0.0, 0.0, 0.0, 0.0, 0.05, 7.05, 14.8, 5.4, 3.6, 2.05, 2.0, 3.1,
              5.95, 8.8, 7.85, 5.4, 3.05, 2.0, 2.3, 3.3, 5.5, etc.]}
        # Note: For the sake of clarity, only the first bins of each element
        # are shown here. Here it is evidenced that copper is the electrode
        # element by the fact that all its atoms are concentrated in one bin.
    """
    time_start = datetime.now()
    # Can't use the total number of frames because is unknown. Must be
    # calculated on the fly.

    # Take first chunk and determine different_elements and box_zlength:
    chunk = next(trajectory_chunks_generator)
    different_elements = get_elements(chunk)
    # box size of trajectory in z direction in Å
    box_zlength = chunk.unitcell_lengths[0][2] * 10

    # Create dictionary with the empty lists for the number of atoms of each
    # element in each zbin
    number_atoms_elements_allframes = {
        different_elements[index_element].name: [0] * int(zbins_number)
        for index_element in range(len(different_elements))
    }

    counter_frames_total = 0.0
    # Here I count the atoms for the first trajectory chunk
    num_frames = len(chunk)
    counter_frames = 0
    for frame in chunk:
        counter_frames += 1.0

        # Iteration for all atoms in frame
        for atom_index in range(frame.n_atoms):

            # takes z coordinate of the atom in Å
            atom_zcoordinate = frame.xyz[0, atom_index, 2] * 10
            # determines to which bin it corresponds
            zbin_id = int((atom_zcoordinate / box_zlength) * zbins_number)
            # determines element name of the atom
            name_element = frame.topology.atom(atom_index).element.name

            # adds 1 atom to the counting normalized to the number of frames
            (number_atoms_elements_allframes[name_element][zbin_id]) += 1 / num_frames

        if counter_frames == 1.0:
            time_iteration = datetime.now() - time_start
            #            total_time = time_iteration * num_frames
            print(f"{time_iteration} (hh:mm:ss.ss) for one frame. \n")
    counter_frames_total += counter_frames

    # Iteration to count the atoms of the rest of trajectory chunks
    counter_chunks = 1.0
    for chunk in trajectory_chunks_generator:
        counter_chunks += 1.0

        # Iteration for all frames in chunk
        num_frames = len(chunk)
        counter_frames = 0
        for frame in chunk:
            counter_frames += 1.0

            # Iteration for all atoms in frame
            for atom_index in range(frame.n_atoms):

                # takes z coordinate of the atom in Å
                atom_zcoordinate = frame.xyz[0, atom_index, 2] * 10
                # determines to which bin it corresponds
                zbin_id = int((atom_zcoordinate / box_zlength) * zbins_number)
                # determines element name of the atom
                name_element = frame.topology.atom(atom_index).element.name

                # adds 1 atom to the counting normalized to the frame number
                (number_atoms_elements_allframes[name_element][zbin_id]) += (
                    1 / num_frames
                )
        counter_frames_total += counter_frames

    print(
        f"Number of chunks:  {counter_chunks}   "
        f"Total number of frames:  {counter_frames_total}"
    )
    # Normalizes to the final number of chunks that have been counted
    for element in number_atoms_elements_allframes:
        for zbin_index in range(len(number_atoms_elements_allframes[element])):
            (number_atoms_elements_allframes[element][zbin_index]) /= counter_chunks

    real_time = datetime.now() - time_start
    print(f"Actual running time: {real_time} (hh:mm:ss.ss)")
    return number_atoms_elements_allframes


def count_number_atoms_types(traj, zbins_number):
    """Counts the number of atoms of each type along the z axis.

    This function determines the different atom types in the simulation,
    divides the simulation box in the specified number of bins
    along the z axis, counts how many atoms of each atom type are in each bin,
    and then averages them for all frames in the trajectory.

    This is the demanding calculation in the process, and it can take a while
    for large trajectories or when performed in systems with low resources.

    Parameters:
        traj (trajectory): The loaded simulation trajectory.
        zbins_number (int):  Total number of bins in the z axis in which the
          simulation box has been divided.

    Returns:
        number_atoms_types_allframes (dictionary of dictionaries): Dictionary
        with the molecules present in the simulation, each with its dictionary
        of atom types in that molecule, and where each atom type has a list
        with the number of atoms located in each z bin.

    Example:
    ::

        count_number_atoms_types(traj, zbins_number)

        >>> 0:00:00.523685 (hh:mm:ss.ss) for one frame.
            There are 25 frames left.
            Estimated running time: 0:00:13.615810 (hh:mm:ss.ss)

            Actual running time: 0:00:14.994484 (hh:mm:ss.ss)

            {'CU':
             {'CU':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               400.00000000005474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, etc.]},
             'CU1':
             {'CU1':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, etc.]},
             'bmim':
             {'C1':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.11538461538461539, 2.26923076923077, 14.230769230769189,
               9.961538461538435, 6.769230769230754, etc.],
              'C10':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.11538461538461539, 7.115384615384599, 15.461538461538415,
               5.923076923076911, 3.5384615384615348, 2.9230769230769216,
               etc.],
              'C2':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 4.076923076923071, 13.461538461538423,
               8.269230769230749, 5.653846153846143, 4.538461538461531, etc.],
              'C3':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.11538461538461539, 4.076923076923071, 12.730769230769194,
               8.769230769230747, 6.1153846153846025, 2.8846153846153832,
               etc.],
              'C4':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 6.653846153846139, 15.692307692307645,
               5.692307692307681, 2.6923076923076916, 2.5384615384615383, etc],
              'C5':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 7.7692307692307505, 15.769230769230722, 5.576923076923066,
               2.73076923076923, 1.8461538461538476, 2.4615384615384617, etc.],
              'C6':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 4.538461538461531, 14.269230769230727, 8.807692307692285,
               3.2692307692307665, 3.3846153846153815, 2.846153846153845,
               etc.],
              'C7':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.2692307692307693, 8.538461538461517, 15.769230769230722,
               7.346153846153829, 1.7692307692307705, 1.307692307692308, etc.],
              'H':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 1.1153846153846152, 5.192307692307683,
               7.961538461538442, 5.076923076923068, 5.461538461538451, etc.],
              'H11':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 0.6153846153846152, 4.384615384615378,
               6.038461538461526, 3.7307692307692264, 4.192307692307686, etc.],
              'H12':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 0.6153846153846152, 4.730769230769223,
               7.269230769230752, 4.5769230769230695, 3.6538461538461497,
               etc.],
              'H13':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 1.5000000000000007, 6.1153846153846025,
               6.999999999999984, 3.461538461538458, 3.6538461538461497, etc.],
              'H14':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 1.384615384615385, 5.423076923076913,
               6.807692307692292, 3.346153846153843, 3.692307692307688, etc.],
              'H15':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               1.2692307692307694, 5.961538461538449, 6.1153846153846025,
               3.19230769230769, 3.7692307692307647, 6.2307692307692175, etc.],
              'H16':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 1.038461538461538, 5.769230769230758,
               6.076923076923064, 3.1538461538461515, 3.6538461538461497,
               etc.],
              'H17':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 0.2692307692307693, 2.846153846153845,
               9.307692307692284, 8.961538461538439, 4.884615384615376, etc],
              'H18':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.23076923076923078, 3.5384615384615348, 10.807692307692278,
               8.730769230769209, 4.6923076923076845, 2.5769230769230766,
               etc.],
              'H19':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.15384615384615385, 1.038461538461538, 6.1153846153846025,
               7.807692307692289, 5.423076923076913, 6.153846153846141, etc.],
              'H20':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 0.9999999999999996, 5.576923076923066,
               8.423076923076902, 4.6923076923076845, 6.423076923076909, etc.],
              'H21':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 1.4615384615384621, 6.3461538461538325,
               7.7692307692307505, 4.9230769230769145, 5.769230769230758,
               etc.],
              'H22':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.15384615384615385, 0.8846153846153842, 5.807692307692296,
               7.692307692307674, 6.038461538461526, 5.653846153846143, etc.],
              'H23':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 1.0769230769230766, 5.346153846153836,
               7.153846153846137, 6.038461538461526, 5.730769230769219, etc.],
              'H24':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.15384615384615385, 3.076923076923075, 6.538461538461524,
               7.615384615384597, 6.269230769230756, 5.999999999999988, etc.],
              'N8':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               2.5, 19.84615384615396, 8.538461538461517, 3.1538461538461515,
               1.8461538461538476, 0.8076923076923074, 0.7307692307692305,
               etc.],
              'N9':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               1.5769230769230778, 16.999999999999993, 9.15384615384613,
               4.9230769230769145, 1.7692307692307705, 1.5769230769230778,
               etc.]},
             'BR':
             {'BR':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 7.269230769230752, 14.42307692307688,
               6.076923076923064, 2.73076923076923, 2.384615384615385, etc.]}}

        # Note: For the sake of clarity, only the first bins of each atom type
        # are shown here. Here it is evidenced that 'CU' is the atom associated
        # with the left electrode by the fact that all its atoms are
        # concentrated in one bin near the origin of the z axis, and 'CU1' is
        # the atom type associated to the right electrode for the same reason
        # but in this case in a bin near the other edge of the simulation box
        # (not visible).
    """
    time_start = datetime.now()

    # Take trajectory and determine box_zlength
    # (yields box size of trajectory in z direction in Å):
    box_zlength = traj.unitcell_lengths[0][2] * 10

    # Create dictionary for every molecule or single atom
    list_atom_types = find_atom_types(traj)

    # Create dictionary with the empty lists for the number of atoms in each
    # zbin of each atom type in each molecule
    number_atoms_types_allframes = copy.deepcopy(list_atom_types)
    for molecule in number_atoms_types_allframes:
        (number_atoms_types_allframes[molecule]) = {
            atom_label: [0] * int(zbins_number)
            for atom_label in number_atoms_types_allframes[molecule]
        }

    # Iteration for all frames
    num_frames = len(traj)
    counter_frames = 0

    for frame in traj:
        counter_frames += 1.0

        # Iteration for all atoms in frame
        for atom_index in range(frame.n_atoms):

            # takes z coordinate of the atom in Å
            atom_zcoordinate = frame.xyz[0, atom_index, 2] * 10
            # determines to which bin it corresponds
            zbin_id = int((atom_zcoordinate / box_zlength) * zbins_number)
            name_molecule = frame.topology.atom(atom_index).residue.name
            name_atom_type = frame.topology.atom(atom_index).name

            # adds 1 atom to the counting normalized to the number of frames
            (number_atoms_types_allframes[name_molecule][name_atom_type][zbin_id]) += (
                1 / num_frames
            )

        if counter_frames == 1.0:
            time_iteration = datetime.now() - time_start
            total_time = time_iteration * num_frames
            print(
                f"{time_iteration} (hh:mm:ss.ss) for one frame. \n"
                f"There are {num_frames-1} frames left. \n"
                f"Estimated running time: {total_time} (hh:mm:ss.ss)\n"
            )

    real_time = datetime.now() - time_start
    print(f"Actual running time: {real_time} (hh:mm:ss.ss)\n")
    return number_atoms_types_allframes


def count_number_atoms_types_chunks(trajectory_chunks_generator, zbins_number):
    """Counts the number of atoms of each type along the z axis by chunks.

    This function determines the different atom types in the simulation,
    divides the simulation box in the specified number of bins
    along the z axis, counts how many atoms of each atom type are in each bin,
    and then averages them for all frames in the trajectory.

    However, instead of calculating this by loading the whole trajectory at
    once, it allows to use a trajectory chunk generator that loads the
    trajectory bit by bit only a few frames at a time.

    This is the demanding calculation in the process, and it can take a while
    for large trajectories or when performed in systems with low resources. But
    with this function you can ensure that, even if it takes a long time, at
    least the calculation can be performed in modest systems, where the memory
    could otherwise become easily overloaded.

    Parameters:
        trajectory_chunks_generator (trajectory generator): Generator that
            yields the trajectory frames in smaller chunks.
        zbins_number (int): Total number of bins in the z axis in which the
          simulation box has been divided.

    Returns:
        number_atoms_types_allframes (dictionary of dictionaries): Dictionary
        with the molecules present in the simulation, each with its dictionary
        of atom types in that molecule, and where each atom type has a list
        with the number of atoms located in each z bin.

    Example:
    ::

        count_number_atoms_types_chunks(trajectory_chunks, zbins_number)

        >>> 0:00:00.523685 (hh:mm:ss.ss) for one frame.
            There are 25 frames left.
            Estimated running time: 0:00:13.615810 (hh:mm:ss.ss)

            Actual running time: 0:00:14.994484 (hh:mm:ss.ss)

            {'CU':
             {'CU':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               400.00000000005474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, etc.]},
             'CU1':
             {'CU1':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, etc.]},
             'bmim':
             {'C1':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.11538461538461539, 2.26923076923077, 14.230769230769189,
               9.961538461538435, 6.769230769230754, etc.],
              'C10':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.11538461538461539, 7.115384615384599, 15.461538461538415,
               5.923076923076911, 3.5384615384615348, 2.9230769230769216,
               etc.],
              'C2':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 4.076923076923071, 13.461538461538423,
               8.269230769230749, 5.653846153846143, 4.538461538461531, etc.],
              'C3':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.11538461538461539, 4.076923076923071, 12.730769230769194,
               8.769230769230747, 6.1153846153846025, 2.8846153846153832,
               etc.],
              'C4':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 6.653846153846139, 15.692307692307645,
               5.692307692307681, 2.6923076923076916, 2.5384615384615383, etc],
              'C5':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 7.7692307692307505, 15.769230769230722, 5.576923076923066,
               2.73076923076923, 1.8461538461538476, 2.4615384615384617, etc.],
              'C6':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 4.538461538461531, 14.269230769230727, 8.807692307692285,
               3.2692307692307665, 3.3846153846153815, 2.846153846153845,
               etc.],
              'C7':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.2692307692307693, 8.538461538461517, 15.769230769230722,
               7.346153846153829, 1.7692307692307705, 1.307692307692308, etc.],
              'H':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 1.1153846153846152, 5.192307692307683,
               7.961538461538442, 5.076923076923068, 5.461538461538451, etc.],
              'H11':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 0.6153846153846152, 4.384615384615378,
               6.038461538461526, 3.7307692307692264, 4.192307692307686, etc.],
              'H12':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 0.6153846153846152, 4.730769230769223,
               7.269230769230752, 4.5769230769230695, 3.6538461538461497,
               etc.],
              'H13':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 1.5000000000000007, 6.1153846153846025,
               6.999999999999984, 3.461538461538458, 3.6538461538461497, etc.],
              'H14':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 1.384615384615385, 5.423076923076913,
               6.807692307692292, 3.346153846153843, 3.692307692307688, etc.],
              'H15':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               1.2692307692307694, 5.961538461538449, 6.1153846153846025,
               3.19230769230769, 3.7692307692307647, 6.2307692307692175, etc.],
              'H16':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 1.038461538461538, 5.769230769230758,
               6.076923076923064, 3.1538461538461515, 3.6538461538461497,
               etc.],
              'H17':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 0.2692307692307693, 2.846153846153845,
               9.307692307692284, 8.961538461538439, 4.884615384615376, etc],
              'H18':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.23076923076923078, 3.5384615384615348, 10.807692307692278,
               8.730769230769209, 4.6923076923076845, 2.5769230769230766,
               etc.],
              'H19':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.15384615384615385, 1.038461538461538, 6.1153846153846025,
               7.807692307692289, 5.423076923076913, 6.153846153846141, etc.],
              'H20':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 0.9999999999999996, 5.576923076923066,
               8.423076923076902, 4.6923076923076845, 6.423076923076909, etc.],
              'H21':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 1.4615384615384621, 6.3461538461538325,
               7.7692307692307505, 4.9230769230769145, 5.769230769230758,
               etc.],
              'H22':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.15384615384615385, 0.8846153846153842, 5.807692307692296,
               7.692307692307674, 6.038461538461526, 5.653846153846143, etc.],
              'H23':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.07692307692307693, 1.0769230769230766, 5.346153846153836,
               7.153846153846137, 6.038461538461526, 5.730769230769219, etc.],
              'H24':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.15384615384615385, 3.076923076923075, 6.538461538461524,
               7.615384615384597, 6.269230769230756, 5.999999999999988, etc.],
              'N8':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               2.5, 19.84615384615396, 8.538461538461517, 3.1538461538461515,
               1.8461538461538476, 0.8076923076923074, 0.7307692307692305,
               etc.],
              'N9':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               1.5769230769230778, 16.999999999999993, 9.15384615384613,
               4.9230769230769145, 1.7692307692307705, 1.5769230769230778,
               etc.]},
             'BR':
             {'BR':
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0.038461538461538464, 7.269230769230752, 14.42307692307688,
               6.076923076923064, 2.73076923076923, 2.384615384615385, etc.]}}

        # Note: For the sake of clarity, only the first bins of each atom type
        # are shown here. Here it is evidenced that 'CU' is the atom associated
        # with the left electrode by the fact that all its atoms are
        # concentrated in one bin near the origin of the z axis, and 'CU1' is
        # the atom type associated to the right electrode for the same reason
        # but in this case in a bin near the other edge of the simulation box
        # (not visible).
    """
    time_start = datetime.now()

    # Can't use the total number of frames because is unknown. Must be
    # calculated on the fly.

    # Take first chunk and determine box_zlength:
    chunk = next(trajectory_chunks_generator)
    # box size of trajectory in z direction in Å
    box_zlength = chunk.unitcell_lengths[0][2] * 10

    # Create dictionary for every molecule or single atom
    list_atom_types = find_atom_types(chunk)

    # Create dictionary with the empty lists for the number of atoms in each
    # zbin of each atom type in each molecule
    number_atoms_types_allframes = copy.deepcopy(list_atom_types)
    for molecule in number_atoms_types_allframes:
        (number_atoms_types_allframes[molecule]) = {
            atom_label: [0] * int(zbins_number)
            for atom_label in number_atoms_types_allframes[molecule]
        }

    counter_frames_total = 0.0
    # Here I count the atoms for the first trajectory chunk
    num_frames = len(chunk)
    counter_frames = 0
    for frame in chunk:
        counter_frames += 1.0

        # Iteration for all atoms in frame
        for atom_index in range(frame.n_atoms):

            # takes z coordinate of the atom in Å
            atom_zcoordinate = frame.xyz[0, atom_index, 2] * 10
            # determines to which bin it corresponds
            zbin_id = int((atom_zcoordinate / box_zlength) * zbins_number)
            name_molecule = frame.topology.atom(atom_index).residue.name
            name_atom_type = frame.topology.atom(atom_index).name

            # adds 1 atom to the counting normalized to the number of frames
            (number_atoms_types_allframes[name_molecule][name_atom_type][zbin_id]) += (
                1 / num_frames
            )

        if counter_frames == 1.0:
            time_iteration = datetime.now() - time_start
            #            total_time = time_iteration * num_frames
            print(f"{time_iteration} (hh:mm:ss.ss) for one frame. \n")

    counter_frames_total += counter_frames

    # Iteration to count the atoms of the rest of trajectory chunks
    counter_chunks = 1.0
    for chunk in trajectory_chunks_generator:
        counter_chunks += 1.0

        # Iteration for all frames in chunk
        num_frames = len(chunk)
        counter_frames = 0
        for frame in chunk:
            counter_frames += 1.0

            # Iteration for all atoms in frame
            for atom_index in range(frame.n_atoms):

                # takes z coordinate of the atom in Å
                atom_zcoordinate = frame.xyz[0, atom_index, 2] * 10
                # determines to which bin it corresponds
                zbin_id = int((atom_zcoordinate / box_zlength) * zbins_number)
                name_molecule = frame.topology.atom(atom_index).residue.name
                name_atom_type = frame.topology.atom(atom_index).name

                # adds 1 atom to the counting normalized to the frame number
                (
                    number_atoms_types_allframes[name_molecule][name_atom_type][zbin_id]
                ) += (1 / num_frames)

        counter_frames_total += counter_frames

    print(
        f"Number of chunks: {counter_chunks}  "
        f"Total number of frames: {counter_frames_total}"
    )

    # Normalizes to the final number of chunks that have been counted
    for residue in number_atoms_types_allframes:
        for atom_type in number_atoms_types_allframes[residue]:
            for zbin_index in range(
                len(number_atoms_types_allframes[residue][atom_type])
            ):
                (
                    number_atoms_types_allframes[residue][atom_type][zbin_index]
                ) /= counter_chunks

    real_time = datetime.now() - time_start
    print(f"Actual running time: {real_time} (hh:mm:ss.ss)\n")
    return number_atoms_types_allframes


def calculate_number_density_elements(number_atoms_elements_allframes, zbin_volume):
    """Calculates the atomic number density for each element along the z axis.

    This function computes the number density of atoms as a function of the z
    axis for each element by taking the number of atoms for each element and
    dividing it by the volume of the z bin.

    Parameters:
        number_atoms_elements_allframes (dictionary of lists): Dictionary with
            the elements present in the simulation, each with its list of the
            number of atoms located in each z bin.
        zbin_volume (float): Volume of each thin bin layer in Å^3, taking into
            account the length of the simulation box in the direction of the x
            and y axis and the bin thickness in the direction of the z axis
            'zbin_interval'.

    Returns:
        number_density_elements_allframes (dictionary of lists): Dictionary
        with the elements present in the simulation, each with its list of the
        number density of atoms in each z bin.

    See Also:
        :func:`md2reflect.count_number_atoms`
        :func:`md2reflect.count_number_atoms_chunks`
    """
    number_density_elements_allframes = {}
    for key in number_atoms_elements_allframes:
        (number_density_elements_allframes[key]) = [
            bin / (zbin_volume) for bin in number_atoms_elements_allframes[key]
        ]  # in Å^-3
    return number_density_elements_allframes


def calculate_number_density_atom_types(number_atoms_types_allframes, zbin_volume):
    """Calculates the atomic number density for each atom type along the z
    axis.

    This function computes the number density of atoms as a function of the z
    axis for each atom type by taking the number of atoms for each atom type
    and dividing it by the volume of the z bin.

    Parameters:
        number_atoms_types_allframes (dictionary of lists): Dictionary with
            the molecules present in the simulation, each with its dictionary
            of atom types in that molecule, and where each atom type has a list
            with the number of atoms located in each z bin.
        zbin_volume (float): Volume of each thin bin layer in Å^3, taking into
            account the length of the simulation box in the direction of the x
            and y axis and the bin thickness in the direction of the z axis
            'zbin_interval'.

    Returns:
        number_density_individually_allframes (dictionary of dictionaries):
            Dictionary with the molecules present in the simulation, each with
            its dictionary of atom types in that molecule, and where each atom
            type has a list with of the number density of atoms in each z bin.

    See Also:
        :func:`md2reflect.count_number_atoms_types`
        :func:`md2reflect.count_number_atoms_types_chunks`
    """
    number_density_individually_allframes = {}
    for residue in number_atoms_types_allframes:
        number_density_individually_allframes[residue] = {}

        for atom_type in number_atoms_types_allframes[residue]:
            number_density_individually_allframes[residue][atom_type] = [
                bin / (zbin_volume)
                for bin in number_atoms_types_allframes[residue][atom_type]
            ]  # in Å^-3

    return number_density_individually_allframes
