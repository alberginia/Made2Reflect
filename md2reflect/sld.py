import copy


def calculate_SLD_elements(number_density_elements_allframes, bc_table):
    """Calculates the SLD of each element in the simulation along the z axis.

    This function calculates the contributions of each element in the
    trajectory to the total neutron or x-ray scattering lenght density.

    The result will be the regarding neutron scattering lenght density or x-ray
    scattering length density, depending on whether the 'bc_table' contains the
    neutron or the x-ray scattering lengths.

    Parameters:
        number_density_elements_allframes (dictionary of lists): Dictionary
            with the elements present in the simulation, each with its list
            of the number density of atoms in each z bin.
        bc_table (dict of tuples): Dictionary of all elements present in the
            simulation with their symbol, real part of the scattering length,
            and also the imaginary part of the scattering length, which
            corresponds to the absorption scattering length).

    Returns:
        SLD_elements (dictionary of lists): Dictionary with the elements
        present in the simulation, each with a list of the real part of its
        scattering length density in each z bin and another similar list with
        the imaginary part of its scattering length density.
    """
    SLD_elements = {}

    # bc_table is in fm, and number_density_elements_allframes is in Å^-3,
    # 1 Å = 10^5 fm
    # SLD_element is in Å^-2
    for key in number_density_elements_allframes:
        SLD_elements[key] = (
            [
                bin * bc_table[key][1] * 10 ** (-5)
                for bin in number_density_elements_allframes[key]
            ],
            [
                bin * bc_table[key][2] * 10 ** (-5)
                for bin in number_density_elements_allframes[key]
            ],
        )
    return SLD_elements


def calculate_SLD_atom_types(number_density_atom_types_allframes, bc_table_atom_types):
    """Calculates the SLD of each atom type in the simulation along the z axis.

    This function calculates the contributions of each atom type in the
    trajectory to the total neutron or x-ray scattering lenght density.

    The result will be the regarding neutron scattering lenght density or x-ray
    scattering length density, depending on whether the 'bc_table_atom_types'
    contains the neutron or the x-ray scattering lengths.

    Parameters:
        number_density_atom_types_allframes (dictionary of dictionaries):
            Dictionary with the molecules present in the simulation, each with
            its dictionary of atom types in that molecule, and where each atom
            type has a list with its number density of atoms in each z bin.

        bc_table_atom_types (dict of dicts): Dictionary of the molecules
            present in the trajectory, each with a dictionary of the atom
            types in that molecule, and where each atom type has a tuple with
            its element symbol, the real part of the scattering length, and
            the imaginary part of the scattering length (which corresponds to
            the absorption scattering length).

    Returns:
        SLD_atom_types (dictionary of dictionaries of lists): Dictionary with
        the molecules present in the simulation, each with its dictionary of
        atom types in that molecule, and where each atom type has a list with
        the real part of its scattering length density in each z bin and
        another similar list with the imaginary part of its scattering length
        density.
    """
    SLD_atom_types = copy.deepcopy(bc_table_atom_types)

    # bc_table is in fm, and number_density_elements_allframes is in Å^-3,
    # 1 Å = 10^5 fm
    # SLD_element is in Å^-2
    for residue in bc_table_atom_types:
        for atom_type in bc_table_atom_types[residue]:
            (SLD_atom_types[residue][atom_type]) = (
                [
                    bin * bc_table_atom_types[residue][atom_type][1] * 10 ** (-5)
                    for bin in number_density_atom_types_allframes[residue][atom_type]
                ],
                [
                    bin * bc_table_atom_types[residue][atom_type][2] * 10 ** (-5)
                    for bin in number_density_atom_types_allframes[residue][atom_type]
                ],
            )

    return SLD_atom_types


def calculate_SLD_total(SLD_elements, substrate_element):
    """Calculates the total SLD of the simulation along the z axis.

    This function computes the total scattering length density of the
    simulation along the z axis by adding the scattering length density
    contributions of the different elements present in the simulation.

    It also computes the total of the scattering length density with all
    contributions except of one of the elements. Typically, this is the
    substrate element.

    It is useful to be able to remove the substrate contribution from the
    simulation scattering length density because the semi-infinite nature of
    the substrate layer cannot be properly represented in the simulation.
    Hence, it is sometimes removed and substituted for a theoretical
    substrate layer.

    Parameters:
        SLD_elements (dictionary of lists): Dictionary with the elements
            present in the simulation, each with the real part of its
            scattering length density in each z bin and another similar list
            with the imaginary part of its scattering length density.
        substrate_element (str): Name of the element of the substrate, such as
            "copper", "silicon", or the like.

    Returns:
        SLD_total and SLD_total_no_substrate (tuple of lists): Total scattering
        length density of the simulation in each z bin (real and imaginary
        parts), and total scattering length without the contribution of the
        substrate element (real and imaginary parts).
    """
    for key in SLD_elements:
        zbins_number = len(SLD_elements[key][0])

    SLD_total_r = [0] * zbins_number
    SLD_total_i = [0] * zbins_number

    for key in SLD_elements:
        for zbin_id in range(len(SLD_elements[key][0])):
            SLD_total_r[zbin_id] += SLD_elements[key][0][zbin_id]
            SLD_total_i[zbin_id] += SLD_elements[key][1][zbin_id]

    # Calculate SLD total without the substrate element

    SLD_total_no_substrate_r = [0] * zbins_number
    SLD_total_no_substrate_i = [0] * zbins_number

    for key in SLD_elements:
        if key is not substrate_element:
            for zbin_id in range(len(SLD_elements[key][0])):
                SLD_total_no_substrate_r[zbin_id] += SLD_elements[key][0][zbin_id]
                SLD_total_no_substrate_i[zbin_id] += SLD_elements[key][1][zbin_id]
    SLD_total = (SLD_total_r, SLD_total_i)
    SLD_total_no_substrate = (SLD_total_no_substrate_r, SLD_total_no_substrate_i)

    return SLD_total, SLD_total_no_substrate


def calculate_SLD_total_atom_types(SLD_atom_types, substrate_residues=None):
    """Computes the total SLD of the simulation along the z axis.

    This function computes the total scattering length density of the
    simulation along the z axis by adding the scattering length density
    contributions of the different atom types present in the simulation.

    It also computes the total of the scattering length density with all
    contributions except of the specified residues/molecules. Typically, the
    latter correspond the substrate element.

    It is useful to be able to remove the substrate contribution from the
    simulation scattering length density because the semi-infinite nature of
    the substrate layer cannot be properly represented in the simulation.
    Hence, it is sometimes removed and substituted for a theoretical substrate
    layer.

    Parameters:
        SLD_atom_types (dictionary of dictionaries of lists): Dictionary with
            the molecules present in the simulation, each with its dictionary
            of atom types in that molecule, and where each atom type has a list
            with the real part of its scattering length density in each z bin
            and another similar list with the imaginary part of its scattering
            length density.
        substrate_residues (list of str): Name of the residues/molecules
            corresponding to the substrate, such as "CU", "SI", or the like.
            Default value is None.

    Returns:
        SLD_total and SLD_total_no_substrate (tuple of lists): Total
        scattering length density of the simulation in each z bin (real and
        imaginary parts), and total scattering length without the contribution
        of the substrate residues/molecules (real and imaginary parts).

    Example:
    ::

        substrate_residues = ["CU", "CU1"]

        (SLD_total_atom_types,
         SLD_total_no_substrate_atom_types) = (calculate_SLD_total_atom_types
                                               (SLD_atom_types,
                                                substrate_residues)
    """
    for residue in SLD_atom_types:
        for atom_type in SLD_atom_types[residue]:
            zbins_number = len(SLD_atom_types[residue][atom_type][0])

    SLD_total_r = [0] * zbins_number
    SLD_total_i = [0] * zbins_number

    for residue in SLD_atom_types:
        for atom_type in SLD_atom_types[residue]:
            for zbin_id in range(len(SLD_atom_types[residue][atom_type][0])):
                SLD_total_r[zbin_id] += SLD_atom_types[residue][atom_type][0][zbin_id]
                SLD_total_i[zbin_id] += SLD_atom_types[residue][atom_type][1][zbin_id]
    SLD_total = (SLD_total_r, SLD_total_i)

    if substrate_residues is not None:
        SLD_total_no_substrate_r = [0] * zbins_number
        SLD_total_no_substrate_i = [0] * zbins_number

        for residue in SLD_atom_types:
            if residue not in substrate_residues:
                for atom_type in SLD_atom_types[residue]:
                    for zbin_id in range(len(SLD_atom_types[residue][atom_type][0])):
                        (SLD_total_no_substrate_r[zbin_id]) += SLD_atom_types[residue][
                            atom_type
                        ][0][zbin_id]
                        (SLD_total_no_substrate_i[zbin_id]) += SLD_atom_types[residue][
                            atom_type
                        ][1][zbin_id]

        SLD_total_no_substrate = (SLD_total_no_substrate_r, SLD_total_no_substrate_i)
        return SLD_total, SLD_total_no_substrate

    return SLD_total


def add_SLD_atom_types(SLD_atom_types, list_atom_types_add):
    """Add SLD contributions of any arbitrary subset of atom types.

    This function adds the contributions from arbitrary subsets of atom types
    to the scattering length density, so the joint contribution of any
    molecular structure or subsets of atoms can be compared to others.

    Parameters:
        SLD_atom_types (dictionary of dictionaries of lists): Dictionary with
            the molecules present in the simulation, each with its dictionary
            of atom types in that molecule, and where each atom type has a list
            with the real part of its scattering length density in each z bin
            and another similar list with the imaginary part of its scattering
            length density.
        list_atom_types_add (dict of lists): Dictionary with all the molecules
            that have atoms that we want to include in the calculation, each
            with a list of the labels of those atoms.

    Returns:
        SLD_added (tuple of lists): Real component of the scattering length
        density contribution of the selected atom types in each z bin, as well
        as the imaginary component.

    Example:
    ::

        list_atom_types_add_ring = {"bmim":
                                    ["C4", "C5", "C6", "C7", "H17", "H18",
                                     "H24", "N8", "N9", "C7", "H20", "H21"]}

        list_atom_types_add_no_ring = {"bmim":
                                       ["C1", "C2", "C3", "C10", "H11",
                                        "H12", "H13", "H14", "H15", "H16",
                                        "H", "H22", "H23"]}

        SLD_ring = add_SLD_atom_types(SLD_atom_types, list_atom_types_add_ring)
        SLD_no_ring = add_SLD_atom_types(SLD_atom_types,
                                         list_atom_types_add_no_ring)
    """
    for residue in list_atom_types_add:
        for atom_type in list_atom_types_add[residue]:
            zbins_number = len(SLD_atom_types[residue][atom_type][0])

    SLD_added_r = [0] * zbins_number
    SLD_added_i = [0] * zbins_number

    for residue in list_atom_types_add:
        for atom_type in list_atom_types_add[residue]:
            for bin_index in range(zbins_number):
                SLD_added_r[bin_index] += SLD_atom_types[residue][atom_type][0][
                    bin_index
                ]
                SLD_added_i[bin_index] += SLD_atom_types[residue][atom_type][1][
                    bin_index
                ]

    SLD_added = (SLD_added_r, SLD_added_i)

    return SLD_added
