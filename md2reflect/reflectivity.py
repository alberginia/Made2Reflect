import copy
import numpy as np

from .abeles_minimal import (
    theoretical_reflectivity,
    simul_reflect_meas_cst_dq_q,
    simul_reflect_meas_monochr,
)


def reflectivity_selector(
    q,
    SLD_scattering,
    SLD_absorption,
    thickness,
    roughness,
    reflectivity_function="theoretical",
    **instrument_parameters
):
    """Wrapper to calculate the reflectivity to allow to choose the function.

    This function calculates a reflectivity profile of a layered material from
    a scattering length density curve (with its real and imaginary parts).

    It can be used both for neutron or x-ray reflectometry simply by
    introducing the proper scattering length density.

    'SLD_scattering' and 'SLD_absorption' should already include the SLD values
    for the substrate and environment layers. Since the first and last values
    correspond to the semi-infinite layers, there is no need to specify any
    thickness for them.

    The reflectivity calculation can be carried out using a purely theoretical
    function, but the 'abeles_minimal' module also allows to take into account
    the resolution.

    Parameters:
        q (numpy array): List with the momentum transfer points (x axis) where
            the reflectivity should be computed (in Å^-1).
        SLD_scattering (numpy array): List with the real component of the
            scattering length density for each layer (in Å^-2).
        SLD_absorption (numpy array): List with the imaginary component of the
            scattering length density for each layer (absorption) (in Å^-2).
        thickness (numpy array): Thickness of each layer (in Å). Must be 0 for
            the first layer and infinite for the last in order to account for
            the semi-infinite nature of those layers.
        roughness (numpy array): Roughness between the layers (in Å).
        reflectivity_function (str): "theoretical" and "abeles_theoretical"
            carry out the calculation assuming a pure Fresnel reflectivity,
            "abeles_constant_resolution" computes the reflectivity curve
            taking into account resolution effects assuming a constant dq/q
            (this is the case at REFSANS), and "abeles_monochromatic" computes
            the reflectivity curve including resolution effects for a
            monochromatic instrument. Default is "theoretical".

    Returns:
        reflectivity_MD (numpy array): Reflectivity profile as a function of the
        momentum transfer (Å^-1) corresponding to the requested scattering
        length density. Output is in the form of a complex number where the
        imaginary part accounts for the absorption.

    """

    if reflectivity_function == "theoretical":
        reflectivity_MD = abs(
            refl(q, SLD_scattering, SLD_absorption, thickness, roughness) ** 2
        )

    elif reflectivity_function == "abeles_theoretical":
        reflectivity_MD = theoretical_reflectivity(
            q, SLD_scattering, SLD_absorption, thickness, roughness
        )

    # "instrument_parameters" names: dq_q, bckg, resol_fn, npts_res, nsigma,
    # normalize, verbose
    elif reflectivity_function == "abeles_constant_resolution":
        reflectivity_MD = simul_reflect_meas_cst_dq_q(
            q,
            SLD_scattering,
            SLD_absorption,
            thickness,
            roughness,
            **instrument_parameters
        )

    # "instrument_parameters" names: wl, dwl, dtheta, bckg, npts_res, nsigma
    elif reflectivity_function == "abeles_monochromatic":
        reflectivity_MD = simul_reflect_meas_monochr(
            q,
            SLD_scattering,
            SLD_absorption,
            thickness,
            roughness,
            **instrument_parameters
        )

    return reflectivity_MD


def calculate_reflectivity(
    SLD_tuned,
    zbin_interval,
    incoming_beam="left",
    left_layers=None,
    right_layers=None,
    q_max=0.25,
    q_number_points=1000,
    reflectivity_function="theoretical",
    **instrument_parameters
):
    """Calculates reflectivity of a SLD with additional layers at both sides.

    This function calculates a reflectivity profile of a layered material from
    a scattering length density curve (with its real and imaginary parts), with
    the option of introducing additional theoretical layers at any of the sides
    of the simulated scattering length density.

    It can be used both for neutron or x-ray reflectometry simply by
    introducing the proper scattering length density.

    'SLD_tuned' should already include the SLD values for the theoretical
    substrate and environment layers. Since the first and last values
    correspond to the semi-infinite layers, there is no need to specify any
    thickness for them.

    However, if more theoretical intermediate layers were added (such as a SiO2
    layer between a silicon substrate and the simulation), we will have to
    provide information of the layer structure in the arguments 'left_layers'
    and/or 'right_layers', so that the proper thickness can be assigned to them
    when calculating the reflectivity.

    Parameters:
        SLD_tuned (tuple of numpy arrays): Constructed scattering length
            density to feed the reflectivity calculation, that combines the
            relevant section of the simulation scattering length density with
            the values of the manually added environment and substrate layers
            (with both, real and imaginary components).
        zbin_interval (float): The thickness of each bin in Å.
        incoming_beam (str): Indicates the income direction of the beam ('left'
            or 'right'). Default value is 'left'.
        left_layers (list of tuples): List with one or more tuple elements
            containing the information of each material layer (real part of the
            the scattering length density, imaginary part of the scattering
            length density, thickness, and roughness) that was added to the
            left of the simulation. The first one is the semi-infinite layer
            and its thickness should be set to zero. The default value is
            None, which means that only the semi-infinite layer was added.
        right_layers (list of tuples): List with one or more tuple
            elements containing the information of each material layer (real
            part of the scattering length density, imaginary part of the
            scattering length density, thickness, and roughness) that was added
            to the right of the simulation. The first one is the semi-infinite
            layer and its thickness should be set to zero. The default value is
            None, which means that only the semi-infinite layer was added.
        q_max (float): Maximum value of momentum transfer (x axis of the
            reflectivity curve) to which calculate the reflectivity. The
            default value is 0.25 Å^-1.
        q_number_points (int): Number of desired points of the reflectivity
            curve (q is the momentum transfer). The default value is 1000
            points.
        reflectivity_function (str): "theoretical" and "abeles_theoretical"
            carry out the calculation assuming a pure Fresnel reflectivity,
            "abeles_constant_resolution" computes the reflectivity curve
            taking into account resolution effects assuming a constant dq/q
            (this is the case at REFSANS), and "abeles_monochromatic" computes
            the reflectivity curve including resolution effects for a
            monochromatic instrument. Default is "theoretical".
        instrument_parameters (dict): A dictionary will all the additional
            instrument parameters that the user needs to specify for certain
            functions to calculate the reflectivity. For the "theoretical" and
            "abeles_theoretical" functions it is not necessary to specify any.
            For the "abeles_constant_resolution" the parameters "dq_q", "bckg",
            "resol_fn", "npts_res", "nsigma", "normalize", "verbose" can be
            specified, and for the "abeles_monochromatic" function the
            parameters "wl", "dwl", "dtheta", "bckg", "npts_res", "nsigma" are
            available. Consult the documentation of the "abeles_minimal" module
            to learn more about each function's parameters. If some or none of
            these parameters are specified, the functions will simply revert to
            their default values.

    Returns:
        reflectivity_MD (numpy array): Reflectivity profile as a function of
        the momentum transfer (Å^-1) corresponding to the requested scattering
        length density.

    """
    # Invert SLD
    if incoming_beam == "right":
        SLD_tuned = invert_SLD(SLD_tuned)

        # Exchanges substrate for environment layers
        temp_left_layers = left_layers
        left_layers = right_layers
        right_layers = temp_left_layers

    elif incoming_beam == "left":
        pass

    # Prepare the thickness and roughness of the simulation layers.
    # Thickness in the beginning and at the end corresponding to the
    # semi-infinite layers have to be 0 and infinite, respectively.

    # Create thickness and roughness corresponding to the SLD_tuned
    thickness = np.array(
        [0.0] + [zbin_interval for bin in SLD_tuned[0][1:-1]] + [np.inf]
    )
    roughness = np.array([0.0] * (len(thickness) - 1))

    # Add the left layers (from 1 to len(left_layers))
    if left_layers is not None:
        for layer_index in range(len(left_layers)):
            if layer_index != 0:
                thickness[layer_index] = left_layers[layer_index][2]
            roughness[layer_index] = left_layers[layer_index][3]

    # Add the right layers
    if right_layers is not None:
        for layer_index in range(len(right_layers)):
            if layer_index != 0:
                (thickness[len(thickness) - 1 - layer_index]) = right_layers[
                    layer_index
                ][2]
            (roughness[len(thickness) - 2 - layer_index]) = right_layers[layer_index][3]

    # Prepare the x axis
    q = np.linspace(0, q_max, q_number_points)

    #    reflectivity_MD = abs(
    #        refl(q, SLD_tuned[0], SLD_tuned[1], thickness, roughness) ** 2
    #    )

    reflectivity_MD = reflectivity_selector(
        q,
        SLD_tuned[0],
        SLD_tuned[1],
        thickness,
        roughness,
        reflectivity_function=reflectivity_function,
        **instrument_parameters
    )

    return reflectivity_MD


def invert_SLD(SLD_original):
    """Inverts the SLD to adjust its orientation with respect to the beam.

    The beam comes from the left on the :func:`calculate_reflectivity`
    function, so the :func:'invert_SLD' function can be used to quickly and
    conveniently invert the z axis and make the beam come from the right
    without having to rebuild the layered scattering length density system.

    Parameters:
        SLD_original (tuple of numpy arrays): Constructed scattering length
            density to feed the reflectivity calculation, that combines the
            relevant section of the simulation scattering length density with
            the manually added substrate and environment layers (with both,
            real and imaginary components).

    Returns:
        SLD_inverted (tuple of numpy arrays): Inverted version of the originaly
        constructed scattering length density to feed the reflectivity
        calculation, that combines the relevant section of the simulation
        scattering length density with the manually added substrate and
        environment layers (with both, real and imaginary components).

    Example:
    ::

        SLD_tuned_left = build_SLD_left(SLD_total_no_substrate, 41, 160,
                                        substrate_layers, environment_layers)

        SLD_tuned_left_inverted = invert_SLD(SLD_tuned_left)
    """
    SLD_inverted = copy.deepcopy(SLD_original)
    zbins_number_core = len(SLD_original[0])

    for zbin_index in range(zbins_number_core):
        SLD_inverted[0][zbin_index] = SLD_original[0][
            zbins_number_core - zbin_index - 1
        ]
        SLD_inverted[1][zbin_index] = SLD_original[1][
            zbins_number_core - zbin_index - 1
        ]

    return SLD_inverted


def refl(qz, sld, sld_abs, thickness, roughness):
    """Simulate a reflectivity curve via the Abeles algorithm (from the SLD).

    This function calculates a reflectivity profile of a layered material from
    a list of the layer thicknesses, their scattering length densities (real
    and imaginary parts), and the roughess of the interfaces.

    It can be used both for neutron or x-ray reflectometry simply by
    introducing the proper scattering length density.

    Parameters:
        qz (numpy array): List with the momentum transfer points (x axis) where
            the reflectivity should be computed (in Å^-1).
        sld (numpy array): List with the real component of the scattering
            length density for each layer (in Å^-2).
        sld_abs (numpy array): List with the imaginary component of the
            scattering length density for each layer (absorption) (in Å^-2).
        thickness (numpy array): Thickness of each layer (in Å). Must be 0 for
            the first layer and infinite for the last in order to account for
            the semi-infinite nature of those layers.
        roughness (numpy array): Roughness between the layers (in Å).

    Returns:
        reflectivity (numpy array): Reflectivity profile as a function of the
        momentum transfer (Å^-1) corresponding to the requested scattering
        length density. Output is in the form of a complex number where the
        imaginary part accounts for the absorption.
    """

    kz = qz * 0.5
    kz = kz.astype(np.complex128)

    drho = 4 * np.pi * (sld - sld[0])
    drho_abs = 4 * np.pi * (sld_abs - sld_abs[0])

    r = np.empty(len(sld), dtype=np.complex128)

    reflectivity = np.empty(len(kz), dtype=np.complex128)

    M = np.empty((2, 2), dtype=np.complex128)
    M_temp = np.empty((2, 2), dtype=np.complex128)

    roughness2 = roughness ** 2

    # number of layer excluding atm and subst
    nl = len(sld) - 2

    # phase factor arrays
    ei2qd = np.empty(len(sld), dtype=np.complex128)
    e_i2qd = np.empty(len(sld), dtype=np.complex128)

    for kcount in range(len(kz)):

        kz2 = kz[kcount] ** 2
        # momentum in substrate
        pmp = np.sqrt(kz2 - (drho[nl + 1] + drho_abs[nl + 1] * 1j))
        # we go layers backwards up to the atm
        for layer in range(nl, -1, -1):

            pm = np.sqrt(kz2 - (drho[layer] + drho_abs[layer] * 1j))
            r[layer + 1] = (pm - pmp) / (pm + pmp)
            # the index for roughness is one less...
            r[layer + 1] *= np.exp(-2 * pm * pmp * roughness2[layer])

            opt_path = pm * thickness[layer]

            ei2qd[layer] = np.exp(1j * opt_path)
            e_i2qd[layer] = 1 / ei2qd[layer]
            pmp = pm

        # prepare the transfer matrix and a temp for multiplication,
        M[0, 0] = 1
        M[0, 1] = r[1]
        M[1, 0] = r[1]
        M[1, 1] = 1

        M_temp = M.copy()

        if nl > 0:
            for layer in range(1, nl + 1):
                M_temp[0, 0] = (
                    M[0, 0] * ei2qd[layer] + M[0, 1] * r[layer + 1] * e_i2qd[layer]
                )
                M_temp[0, 1] = (
                    M[0, 0] * r[layer + 1] * ei2qd[layer] + M[0, 1] * e_i2qd[layer]
                )
                M_temp[1, 0] = (
                    M[1, 0] * ei2qd[layer] + M[1, 1] * r[layer + 1] * e_i2qd[layer]
                )
                M_temp[1, 1] = (
                    M[1, 0] * r[layer + 1] * ei2qd[layer] + M[1, 1] * e_i2qd[layer]
                )

                M = M_temp.copy()
        reflectivity[kcount] = M[1, 0] / M[0, 0]
    return reflectivity
