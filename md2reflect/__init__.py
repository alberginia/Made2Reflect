""" MD2Reflect - Version 0.15"""

__author__ = "Muriel Rovira-Esteva"
__version__ = "0.15"


from .general import version, check_compatibility

from .load import load_trajectory, chop_trajectory, get_trajectory_chunk

from .number_density import (
    select_zbinning,
    count_number_atoms,
    count_number_atoms_chunks,
    count_number_atoms_types,
    count_number_atoms_types_chunks,
    calculate_number_density_elements,
    calculate_number_density_atom_types,
)

from .composition import (
    find_atom_types,
    find_atom_types_elements,
    get_elements_bc_neutrons,
    get_elements_b_xrays,
    get_elements,
    get_atom_types_bc_neutrons,
    get_atom_types_b_xrays,
    list_molecules,
    isotopic_substitution,
)

from .sld import (
    calculate_SLD_elements,
    calculate_SLD_atom_types,
    calculate_SLD_total,
    calculate_SLD_total_atom_types,
    add_SLD_atom_types,
)

from .layers import (
    calculate_substrate_SLD,
    average_environment_SLD,
    build_substrate_layer,
    build_average_environmental_layer,
    build_SLD_left,
    build_SLD_right,
)

from .reflectivity import (
    reflectivity_selector,
    calculate_reflectivity,
    invert_SLD,
    refl,
)

from .save import (
    create_folder,
    save_file,
    save_number_density,
    save_number_density_atom_types,
    save_SLD_elements,
    save_SLD_atom_types,
    save_SLD_atom_types_combination,
    save_SLD_left,
    save_SLD_right,
    save_SLD_total,
    save_reflectivity_left,
    save_reflectivity_right,
    display_residues,
    show_SLD_range,
)

from .abeles_minimal import (
    theoretical_reflectivity,
    simul_reflect_meas_cst_dq_q,
    simul_reflect_meas_monochr,
)


__all__ = [
    "version",
    "check_compatibility",
    "load_trajectory",
    "chop_trajectory",
    "get_trajectory_chunk",
    "select_zbinning",
    "count_number_atoms",
    "count_number_atoms_chunks",
    "count_number_atoms_types",
    "count_number_atoms_types_chunks",
    "calculate_number_density_elements",
    "calculate_number_density_atom_types",
    "find_atom_types",
    "find_atom_types_elements",
    "get_elements_bc_neutrons",
    "get_elements_b_xrays",
    "get_elements",
    "get_atom_types_bc_neutrons",
    "get_atom_types_b_xrays",
    "list_molecules",
    "isotopic_substitution",
    "calculate_SLD_elements",
    "calculate_SLD_atom_types",
    "calculate_SLD_total",
    "calculate_SLD_total_atom_types",
    "add_SLD_atom_types",
    "calculate_substrate_SLD",
    "average_environment_SLD",
    "build_substrate_layer",
    "build_average_environmental_layer",
    "build_SLD_left",
    "build_SLD_right",
    "reflectivity_selector",
    "calculate_reflectivity",
    "invert_SLD",
    "refl",
    "display_residues",
    "show_SLD_range",
    "create_folder",
    "save_file",
    "save_number_density",
    "save_number_density_atom_types",
    "save_SLD_elements",
    "save_SLD_atom_types",
    "save_SLD_atom_types_combination",
    "save_SLD_left",
    "save_SLD_right",
    "save_SLD_total",
    "save_reflectivity_left",
    "save_reflectivity_right",
    "theoretical_reflectivity",
    "simul_reflect_meas_cst_dq_q",
    "simul_reflect_meas_monochr",
]
