import sys
import subprocess
import numpy as np
import matplotlib.pyplot
from IPython import version_info as IPython_version_info
import periodictable
import mdtraj as md


def version():
    """Outputs the version of this module (MD2reflect).

    Returns:
        version (str): Version of the program.
    """
    version = "0.15"
    return version


def check_compatibility():
    """Compares recommended library versions with those available in the system.

    This function compares the recommended versions of the libraries with the
    actual versions found in the local python environment. Although the
    MD2reflect module has been tested with the following library versions, it
    may work with other versions as well:

    * Python         3.7.5
    * Numpy          1.18.0
    * Matplotlib     3.1.2
    * IPython        7.11.0
    * Periodictable  1.5.2
    * MDTraj         1.9.3
    * Pymol          2.2.0

    Parameters:
        None

    Returns:
        None
    """
    try:
        python_version = (
            str(sys.version_info.major)
            + "."
            + str(sys.version_info.minor)
            + "."
            + str(sys.version_info.micro)
        )
    except ImportError:
        python_version = "NO module found"
    except Exception:
        python_version = "Unknown"

    try:
        numpy_version = np.version.version
    except ImportError:
        numpy_version = "NO module found"
    except Exception:
        numpy_version = "Unknown"

    try:
        matplotlib_version = matplotlib.__version__
    except ImportError:
        matplotlib_version = "NO module found"
    except Exception:
        matplotlib_version = "Unknown"

    try:
        ipython_version_tuple = IPython_version_info
        ipython_version = ""
        for num in ipython_version_tuple:
            ipython_version += str(num) + "."
        ipython_version = ipython_version.strip(".")
    except ImportError:
        ipython_version = "NO module found"
    except Exception:
        ipython_version = "Unknown"

    try:
        periodictable_version = periodictable.__version__
    except ImportError:
        periodictable_version = "NO module found"
    except Exception:
        periodictable_version = "Unknown"

    try:
        mdtraj_version = md.version.version
    except ImportError:
        mdtraj_version = "NO module found"
    except Exception:
        mdtraj_version = "Unknown"

    try:
        returned_output = ""
        returned_output = (
            subprocess.check_output("pip freeze | grep pymol", shell=True)
            .decode("utf-8")
            .strip()
        )
        if "pymol" in returned_output:
            pymol_version = returned_output[7:]
        else:
            pymol_version = "NO module found"
    except Exception:
        pymol_version = "Unknown"

    recommended_versions = {
        "Python": "3.7.5",
        "Numpy": "1.18.0",
        "Matplotlib": "3.1.2",
        "IPython": "7.11.0",
        "Periodictable": "1.5.2",
        "MDTraj": "1.9.3",
        "Pymol": "2.2.0",
    }

    actual_versions = {
        "Python": python_version,
        "Numpy": numpy_version,
        "Matplotlib": matplotlib_version,
        "IPython": ipython_version,
        "Periodictable": periodictable_version,
        "MDTraj": mdtraj_version,
        "Pymol": pymol_version,
    }
    space = " "
    print(f"{space*16} Recommended{space*3} Actual{space*4}")
    print(f"{space*16} version{space*7} version{space*3}\n")

    module_list = list(recommended_versions.keys())
    for module in module_list:
        comparison = "<-"
        if recommended_versions[module] == actual_versions[module]:
            comparison = ""
        print(
            f"{module:<16} {recommended_versions[module]:<14} "
            f"{actual_versions[module]:<12} {comparison:<4}"
        )

    print(
        '\nThis module has been tested with the "recommended" library '
        "versions, but it may work with other versions as well."
    )


###############################################
# def main():


# if __name__ == "__main__":
#    main()
