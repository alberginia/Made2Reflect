���-      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Readme File�h]�h �Text����Readme File�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�\/home/muriel/Feina/2019-06-09_Simulació_reflectometria/7_Codi_Gitlab/sphinx/docs/readme.rst�hKubh �	paragraph���)��}�(h�# MD2reflect (v0.15)�h]�h�# MD2reflect (v0.15)�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+h�../README.md�hKhhhhubh,)��}�(hXB  MD2reflect is a scientific software written in Python 3 for the calculation of
neutron and x-ray reflectivity profiles from molecular dynamic simulations. It
is a project of the Helmholtz-Zentrum Geesthacht, German Engineering Materials
Science Centre (GEMS) at Heinz Maier-Leibnitz Zentrum (MLZ), in Garching near
Munich.�h]�hXB  MD2reflect is a scientific software written in Python 3 for the calculation of
neutron and x-ray reflectivity profiles from molecular dynamic simulations. It
is a project of the Helmholtz-Zentrum Geesthacht, German Engineering Materials
Science Centre (GEMS) at Heinz Maier-Leibnitz Zentrum (MLZ), in Garching near
Munich.�����}�(hh>hh<hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hKhhhhubh,)��}�(hX2  The program reads molecular dynamics computer simulation trajectories, can
perform isotopic exchange on any groups of atoms, and calculates the scattering
length density for neutron scattering or the electron density for x-ray
scattering in bins along the z-axis. Two semi-infinite edges (substrate and
bulk) must be added to the scattering length or electron density obtained from
the simulations, which may contain layered structures and be smeared with a
substrate roughness specified by the user. From this, the expected reflectivity
curve can be calculated.�h]�hX2  The program reads molecular dynamics computer simulation trajectories, can
perform isotopic exchange on any groups of atoms, and calculates the scattering
length density for neutron scattering or the electron density for x-ray
scattering in bins along the z-axis. Two semi-infinite edges (substrate and
bulk) must be added to the scattering length or electron density obtained from
the simulations, which may contain layered structures and be smeared with a
substrate roughness specified by the user. From this, the expected reflectivity
curve can be calculated.�����}�(hhLhhJhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK	hhhhubh,)��}�(h��Since it uses the library MDTraj to load the files, it is compatible with a
large range of trajectory file formats (DCD, PDB…).�h]�h��Since it uses the library MDTraj to load the files, it is compatible with a
large range of trajectory file formats (DCD, PDB…).�����}�(hhZhhXhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hKhhhhubh,)��}�(h�eDocumentation and installation instructions are available at
https://alberginia.gitlab.io/MD2reflect/�h]�(h�=Documentation and installation instructions are available at
�����}�(h�=Documentation and installation instructions are available at
�hhfhhhNhNubh �	reference���)��}�(h�(https://alberginia.gitlab.io/MD2reflect/�h]�h�(https://alberginia.gitlab.io/MD2reflect/�����}�(hhhhqubah}�(h]�h!]�h#]�h%]�h']��refuri�hsuh)hohhfubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hKhhhhubh,)��}�(h�7The code is freely available under the GNU GPL License.�h]�h�7The code is freely available under the GNU GPL License.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hKhhhhubh,)��}�(h�### INSTALLATION�h]�h�### INSTALLATION�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hKhhhhubh,)��}�(h��MD2reflect makes use of a series of Python libraries. In order to ensure it
works correctly we recommend installing those versions of the modules
(although it may work with other versions as well):�h]�h��MD2reflect makes use of a series of Python libraries. In order to ensure it
works correctly we recommend installing those versions of the modules
(although it may work with other versions as well):�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hKhhhhubh �block_quote���)��}�(hhh]�h,)��}�(h��Python         3.7.5
Numpy          1.18.0
Matplotlib     3.1.2
IPython        7.11.0
Periodictable  1.5.2
MDTraj         1.9.3
Pymol          2.2.0�h]�h��Python         3.7.5
Numpy          1.18.0
Matplotlib     3.1.2
IPython        7.11.0
Periodictable  1.5.2
MDTraj         1.9.3
Pymol          2.2.0�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK!hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hhhhhh;hNubh,)��}�(h��A Docker image can be built with all the necessary dependencies to run the
package. The necessary files and the instructions for linux can be found in the
"docker-files" folder of the repository.�h]�h��A Docker image can be built with all the necessary dependencies to run the
package. The necessary files and the instructions for linux can be found in the
“docker-files” folder of the repository.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK)hhhhubh,)��}�(hX!  The Sphinx documentation can be rebuilt by running "make html" in the docs
folder. Jupyter notebooks included in the documentation must be updated in the
"docs/examples" folder. The index file as well as the rest of the documentation
files can be found under the "docs/_build/html" folder.�h]�hX-  The Sphinx documentation can be rebuilt by running “make html” in the docs
folder. Jupyter notebooks included in the documentation must be updated in the
“docs/examples” folder. The index file as well as the rest of the documentation
files can be found under the “docs/_build/html” folder.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK-hhhhubh,)��}�(h�### CONTRIBUTION�h]�h�### CONTRIBUTION�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK3hhhhubh,)��}�(h�6This project uses pre-commits to unify the code style.�h]�h�6This project uses pre-commits to unify the code style.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK5hhhhubh,)��}�(h�[If you want to contribute, please follow these steps:
1. Install pre-commit in your system:�h]�h�[If you want to contribute, please follow these steps:
1. Install pre-commit in your system:�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK7hhhhubh�)��}�(hhh]�h,)��}�(h�`pip install pre-commit`�h]�h �title_reference���)��}�(hj  h]�h�pip install pre-commit�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK9hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hhhhhh;hNubh �enumerated_list���)��}�(hhh]�h �	list_item���)��}�(h��Activate pre-commit for this project from the local repository folder (if you use worktree you will have to do this for every branch):
 `pre-commit install`
�h]�h �definition_list���)��}�(hhh]�h �definition_list_item���)��}�(h��Activate pre-commit for this project from the local repository folder (if you use worktree you will have to do this for every branch):
`pre-commit install`
�h]�(h �term���)��}�(h��Activate pre-commit for this project from the local repository folder (if you use worktree you will have to do this for every branch):�h]�h��Activate pre-commit for this project from the local repository folder (if you use worktree you will have to do this for every branch):�����}�(hjK  hjI  ubah}�(h]�h!]�h#]�h%]�h']�uh)jG  hh;hK<hjC  ubh �
definition���)��}�(hhh]�h,)��}�(h�`pre-commit install`�h]�j  )��}�(hj^  h]�h�pre-commit install�����}�(hhhj`  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj\  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK<hjY  ubah}�(h]�h!]�h#]�h%]�h']�uh)jW  hjC  ubeh}�(h]�h!]�h#]�h%]�h']�uh)jA  hh;hK<hj>  ubah}�(h]�h!]�h#]�h%]�h']�uh)j<  hj8  ubah}�(h]�h!]�h#]�h%]�h']�uh)j6  hj3  hhhNhNubah}�(h]�h!]�h#]�h%]�h']��enumtype��arabic��prefix�h�suffix��.��start�Kuh)j1  hhhhhh;hK;ubh,)��}�(hX�  After that, files which are changed and added to the stage will be checked with black and flake8 when trying to commit those changes. If the file is not black compliant, it will be reformatted accordingly and will have to be staged again in order to commit it with those changes. If the file is not flake8 compliant, the warning/s will be shown and the commit will not take place. The file will have to be modified manually in order to fix the flake8 warnings and be able to commit any changes.�h]�hX�  After that, files which are changed and added to the stage will be checked with black and flake8 when trying to commit those changes. If the file is not black compliant, it will be reformatted accordingly and will have to be staged again in order to commit it with those changes. If the file is not flake8 compliant, the warning/s will be shown and the commit will not take place. The file will have to be modified manually in order to fix the flake8 warnings and be able to commit any changes.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh;hK>hhhhubeh}�(h]��readme-file�ah!]�h#]��readme file�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j�  j�  s�	nametypes�}�j�  Nsh}�j�  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]�(h �system_message���)��}�(hhh]�h,)��}�(h�Unexpected indentation.�h]�h�Unexpected indentation.�����}�(hhhj2  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj/  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type��ERROR��source�h;�line�K9uh)j-  hhhhhh;hK8ubj.  )��}�(hhh]�h,)��}�(h�:Enumerated list start value not ordinal-1: "2" (ordinal 2)�h]�h�>Enumerated list start value not ordinal-1: “2” (ordinal 2)�����}�(hhhjN  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hjK  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type��INFO��source�h*�line�Kuh)j-  hhhhhh;hK;ube�transform_messages�]��transformer�N�
decoration�Nhhub.