.. MD2reflect documentation master file, created by
   sphinx-quickstart on Sun May 31 05:20:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to MD2reflect's documentation!
======================================


Neutron and X-ray reflectometry - From simulations to experiments
-----------------------------------------------------------------

MD2reflect is a scientific software written in Python 3 for the calculation of 
neutron and x-ray reflectivity profiles from molecular dynamic simulations. It 
is a project of the `Helmholtz-Zentrum Geesthacht <https://www.hzg.de/>`_, `German Engineering 
Materials Science Centre (GEMS) <https://www.hzg.de/institutes_platforms/gems/index.php.en>`_ at `Heinz Maier-Leibnitz Zentrum (MLZ) <https://www.mlz-garching.de/englisch>`_, in 
Garching near Munich. 

The program reads molecular dynamics computer simulation trajectories, can 
perform isotopic exchange on any groups of atoms, and calculates the 
scattering length density for neutron scattering or the electron density for
x-ray scattering in bins along the z-axis. Two semi-infinite edges (substrate 
and bulk) must be added to the scattering length or electron density obtained 
from the simulations, which may contain layered structures and be smeared with 
a substrate roughness specified by the user. From this, the expected 
reflectivity curve can be calculated.

Since it uses the library `MDTraj <http://mdtraj.org/>`_ to load the files, it is compatible with a 
large range of trajectory file formats (DCD, PDB...).

The code is freely available under the :ref:`GNU GPL License <license-label>`.


.. toctree::
   :hidden:

   Home <self>


   

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   flux
   examples
   license
   md2reflect

   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
