Basic workflux
==============

1.- Load trajectory
-------------------

The trajectory needs to be in a format that contains the topology of the system 
(such as PDB), or we need an additional file that only has to contain the 
topology (such as a single frame of the trajectory in PDB).

There's the possibility to load one or multiple trajectory files simultaneously
to carry out the calculations, or to skip frames if the trajectory is 
unnecessarily large.

To prevent overloading the memory of systems with low resources when handling
large simulation files, it is also possible to load the trajectory some 
frames at a time with the function 
:func:`chop_trajectory() <md2reflect.chop_trajectory()>`.

.. seealso:: 
    :func:`load_trajectory() <md2reflect.load_trajectory()>` 
    :func:`chop_trajectory() <md2reflect.chop_trajectory()>` 
    :func:`get_trajectory_chunk() <md2reflect.get_trajectory_chunk()>` 



2- Compute atomic number density of simulation
----------------------------------------------

We have to count the number of atoms as a function of the z axis. However, 
first we have to decide a **proper binning of the axis**.

A moderate number of thicker bins provides an implicit averaging, with less 
noisy profiles, but also with less visible features. Large bins also pose a 
problem when the border between bins coincides with the middle of a peak, 
because the peak is then shared between the two bins, which can greatly reduce 
its height. This translates into a large variation of peak heights with only 
small variations of the bin thickness, a can be clearly observed in many systems 
for a large range of binning intervals.

Conversely, a large number of thinner bins could provide more details of the 
profile features, and they do not pose so much danger of assymmetrically 
distorting the profile. However, they do not provide such a significant 
averaging and the resulting profiles are sometimes extremely noisy, which makes 
it harder to analyze its features.

We need to **count the atoms separately** so that we can later assign them 
different coherent scattering lengths. There are two options implemented that
we can choose:
 
Element
    By grouping all the atoms that belong to the same element 
    (:func:`count_number_atoms() <md2reflect.count_number_atoms()>` , or 
    :func:`count_number_atoms_chunks() <md2reflect.count_number_atoms_chunks()>`
    if we need to carry out the calculation loading few frames of the trajectory
    at a time). Simpler to manage but with less flexibility. 

Atom type
    By grouping all the atoms that occupy the same site in their molecule
    (:func:`count_number_atoms_types() <md2reflect.count_number_atoms_types()>`, or
    :func:`count_number_atoms_types_chunks() <md2reflect.count_number_atoms_types_chunks()>`
    if we need to carry out the calculation loading few frames of the trajectory
    at a time). Slightly more complex to manage but adaptable to the 
    particularities of each problem.

Once we have counted the atoms, we use one the corresponding function to **compute the
atomic number density**.


.. seealso:: 
    :func:`select_zbinning() <md2reflect.select_zbinning()>` 
    :func:`count_number_atoms() <md2reflect.count_number_atoms()>` 
    :func:`count_number_atoms_chunks() <md2reflect.count_number_atoms_chunks()>` 
    :func:`count_number_atoms_types() <md2reflect.count_number_atoms_types()>` 
    :func:`count_number_atoms_types_chunks() <md2reflect.count_number_atoms_types_chunks()>` 
    :func:`calculate_number_density_elements() <md2reflect.calculate_number_density_elements()>` 
    :func:`calculate_number_density_atom_types() <md2reflect.calculate_number_density_atom_types()>` 



3.- Gather the scattering lengths and carry out isotopic substitution
---------------------------------------------------------------------

Depending on the counting method chosen on the previous step, there are 
different possibilities:

Element
    The scattering lengths of the different elements are obtained using the
    functions 
    :func:`get_elements_bc_neutrons() <md2reflect.get_elements_bc_neutrons()>`
    or :func:`get_elements_b_xrays() <md2reflect.get_elements_b_xrays()>`, 
    depending on whether we are interested on the neutron or the x-ray 
    reflectivity. 
    
    **Isotopic substitution can only be carried out if it takes 
    place simultaneously for ALL atoms of the same element**, regardless of 
    their molecular site. Check
    :func:`get_elements_bc_neutrons() <md2reflect.get_elements_bc_neutrons()>`
    for an example on how to do this.

Atom type
    The scattering lengths of the different atom types are obtained using the
    functions
    :func:`get_atom_types_bc_neutrons() <md2reflect.get_atom_types_bc_neutrons()>`
    or :func:`get_atom_types_b_xrays() <md2reflect.get_atom_types_b_xrays()>`, 
    depending on whether we are interested on the neutron or the x-ray 
    reflectivity. 
    
    **Isotopic substitution can be carried out for single atoms or 
    for any arbitrary combination of atom types**.  Use the functions
    :func:`list_molecules() <md2reflect.list_molecules()>` and 
    :func:`display_residues() <md2reflect.display_residues()>` to visualize and
    identify the molecule and atom labels of the atoms, and then 
    :func:`isotopic_substitution() <md2reflect.isotopic_substitution()>` to 
    specify the details and carry out the isotopic substitution.   


.. seealso:: 
    :func:`find_atom_types_elements() <md2reflect.find_atom_types_elements()>` 
    :func:`get_elements_b_xrays() <md2reflect.get_elements_b_xrays()>` 
    :func:`get_elements_bc_neutrons() <md2reflect.get_elements_bc_neutrons()>` 
    :func:`get_elements() <md2reflect.get_elements()>` 
    :func:`get_atom_types_b_xrays() <md2reflect.get_atom_types_b_xrays()>` 
    :func:`get_atom_types_bc_neutrons() <md2reflect.get_atom_types_bc_neutrons()>` 
    :func:`list_molecules() <md2reflect.list_molecules()>` 
    :func:`display_residues() <md2reflect.display_residues()>` 
    :func:`isotopic_substitution() <md2reflect.isotopic_substitution()>` 


4.- Compute scattering length density of simulation
---------------------------------------------------

Element
    Use :func:`calculate_SLD_elements() <md2reflect.calculate_SLD_elements()>` 
    to compute the scattering length density contribution of each element. Then 
    add all the scattering length density contributions and all contributions 
    except for the one of the element corresponding to the substrate using the function
    :func:`calculate_SLD_total() <md2reflect.calculate_SLD_total()>`. This can 
    only be used if the substrate is composed of only one element and this is 
    not present in the sample, if this is not the case, the whole calculation 
    must be carried out by atom type.

Atom type
    Use :func:`calculate_SLD_atom_types() <md2reflect.calculate_SLD_atom_types()>` 
    to compute the scattering length density contribution of each element. Then 
    add all the scattering length density contributions and all contributions 
    except for the molecules or atom types corresponding to the substrate using 
    the function
    :func:`calculate_SLD_total() <md2reflect.calculate_SLD_total()>`.
    
    Apart from the typical calculations, this separation in the contributions of
    each atom type also allows to **compute the scattering length density 
    contributions of arbitrary groups of atoms** (such as atoms in an aromatic 
    ring versus atoms not in the ring). Identify the molecules, labels 
    and location of the atom types using 
    :func:`find_atom_types() <md2reflect.find_atom_types()>` and  
    :func:`display_residues() <md2reflect.display_residues()>`. And then 
    calculate their joint contribution using 
    :func:`add_SLD_atom_types() <md2reflect.add_SLD_atom_types()>`.
    

.. seealso:: 
    :func:`calculate_SLD_elements() <md2reflect.calculate_SLD_elements()>` 
    :func:`calculate_SLD_total() <md2reflect.calculate_SLD_total()>` 
    :func:`calculate_SLD_atom_types() <md2reflect.calculate_SLD_atom_types()>` 
    :func:`calculate_SLD_total_atom_types() <md2reflect.calculate_SLD_total_atom_types()>` 
    :func:`find_atom_types() <md2reflect.find_atom_types()>` 
    :func:`display_residues() <md2reflect.display_residues()>` 
    :func:`add_SLD_atom_types() <md2reflect.add_SLD_atom_types()>` 


5.- Add theoretical layers to the scattering length density of simulation
-------------------------------------------------------------------------

Build the substrate and environment layers or multilayers, using
:func:`build_substrate_layer() <md2reflect.build_substrate_layer()>` and
:func:`build_average_environmental_layer() <md2reflect.build_average_environmental_layer()>`,
or manually if there are several layers.
The first function simply takes the scattering length value of the indicated 
element, the second makes an average of the scattering length density values of 
the simulation in a certain range (normally what is considered to be close to 
the bulk).

Then, take the total scattering length density of the simulation without the 
substrate contribution and select the relevant range. 

To better visualize and select the ranges of the scattering length density use 
the function :func:`show_SLD_range() <md2reflect.show_SLD_range()>`.

Finally, combine the simulation scattering length density with the theoretical 
layers using the functions 
:func:`build_SLD_left() <md2reflect.build_SLD_left()>` and
:func:`build_SLD_right() <md2reflect.build_SLD_right()>`. 

If several theoretical substrate or environment layers are defined it is 
possible to specify a roughness between their interfaces, except the roughness 
between the substrate and the simulation, which has not been implemented so far, 
so it's a parameter that shouldn't be used, its value must be 0. 


.. seealso:: 
    :func:`calculate_substrate_SLD() <md2reflect.calculate_substrate_SLD()>` 
    :func:`show_SLD_range() <md2reflect.show_SLD_range()>` 
    :func:`average_environment_SLD() <md2reflect.average_environment_SLD()>` 
    :func:`build_substrate_layer() <md2reflect.build_substrate_layer()>` 
    :func:`build_average_environmental_layer() <md2reflect.build_average_environmental_layer()>` 
    :func:`build_SLD_left() <md2reflect.build_SLD_left()>` 
    :func:`build_SLD_right() <md2reflect.build_SLD_right()>` 


6.- Compute reflectivity 
------------------------

Compute the reflectivity using the scattering length density constructed with
the simulation and the theoretical layers, through the function
:func:`calculate_reflectivity() <md2reflect.calculate_reflectivity()>`. The 
direction of the incoming beam along the z axis can be easily indicated. 

The fact that it corresponds to 
neutron or x-ray reflectivity has no significance, because it has already been 
taken into account through the scattering length values (in step 3). 

If there are intermediate layers between the semi-infinite substrate 
or environment and the simulation, they have to be explicitly indicated into 
the reflectivity function.

.. seealso:: 
    :func:`calculate_reflectivity() <md2reflect.calculate_reflectivity()>` 
    :func:`invert_SLD() <md2reflect.invert_SLD()>` 
    :func:`refl() <md2reflect.refl()>`  


7.- Save and display results 
----------------------------

All the functions that display and save the different results, save graphs into the 
subfolder "figures" and datasets into the subfolder "data". Except for the function   
:func:`display_residues() <md2reflect.display_residues()>`, that saves drawings
and PDB files of the molecules in the subfolder "residues". 


.. seealso:: 
    :func:`create_folder() <md2reflect.create_folder()>` 
    :func:`display_residues() <md2reflect.display_residues()>` 
    :func:`save_file() <md2reflect.save_file()>` 
    :func:`save_number_density() <md2reflect.save_number_density()>` 
    :func:`save_number_density_atom_types() <md2reflect.save_number_density_atom_types()>` 
    :func:`save_SLD_elements() <md2reflect.save_SLD_elements()>` 
    :func:`save_SLD_atom_types() <md2reflect.save_SLD_atom_types()>` 
    :func:`save_SLD_atom_types_combination() <md2reflect.save_SLD_atom_types_combination()>`
    :func:`save_SLD_left() <md2reflect.save_SLD_left()>`
    :func:`save_SLD_right() <md2reflect.save_SLD_right()>`
    :func:`save_SLD_total() <md2reflect.save_SLD_total()>`
    :func:`save_reflectivity_left() <md2reflect.save_reflectivity_left()>`
    :func:`save_reflectivity_right() <md2reflect.save_reflectivity_right()>`

